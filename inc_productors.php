<?php

require_once './dbHelper.php';

$sql = "select * from categories";
$rs = load($sql);

while ($row = $rs->fetch_assoc()) {
    $id = $row["CatID"];
    $name = $row["CatName"];
    ?>
            <li role="separator" class="divider"></li>
            <li><a href="listProducts.php?act=products&tab=search&id=<?php echo $id; ?>&name=<?php echo $name; ?>&page=1"><?php echo $name; ?></a></li>
    <?php
}
?>