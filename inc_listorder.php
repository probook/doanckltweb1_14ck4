<?php
require_once './inc_func.php';
require_once './dbHelper.php';

if(isset($_POST["textStatus"]))
{
    $id = $_POST["textID"];
    $status = $_POST["textStatus"];
    $sql = "Update orders set Status = '$status' where OrderID = '$id'";
    $n = save($sql,1);
    redirect("admin.php?act=orders&$id");
}

?>

<?php
    $sql = "select u.f_ID, u.f_Name, o.OrderID, o.OrderDate, o.Total, o.Status from users u, orders o, orderdetails n where u.f_ID = o.UserID and o.OrderID = n.OrderID group by o.OrderID order by o.OrderDate DESC";
    $rs = load($sql);
    $i = 1;
    while ($row = $rs->fetch_assoc()) {
        ?>
        <?php
        if($row["Status"] == "Đã giao")
        {
            ?>
                <tr id="trOrder<?php echo $i; ?>" name="trOrder<?php echo $i; ?>" class="success">
                    <td><?php echo $i; ?></td>
                    <td><?php echo $row["f_Name"]; ?></td>
                    <?php
                        $str_day = $row["OrderDate"];
                        $day = strtotime($str_day);
                        $str_day = date('d-m-Y',$day); 
                     ?>
                    <td><?php echo $str_day; ?></td>
                    <td><?php echo number_format($row["Total"]); ?></td>               
                    <td colspan="3">
                            <select class="form-control" name="txtStatus<?php echo $i; ?>" id="txtStatus<?php echo $i; ?>" onchange="changeColor(<?php echo $i; ?>)" value="<?php echo $row["Status"]; ?>">
                                <option  value="<?php echo $row["Status"]; ?>"><?php echo $row["Status"]; ?></option>
                                <option  value="Chưa giao">Chưa giao</option>
                            </select>
                    </td>
                    <form name="frmStatus" id="frmStatus" action="" method="post">
                        <input type="hidden" name="textID" id="textID" value="<?php echo $row["OrderID"]; ?>"/>
                        <input type="hidden" name="textStatus" id="textStatus" value=""/>
                    </form>
                    <td>
                        <a href="admin.php?act=orders&view=details&id=<?php echo $row["OrderID"]; ?>" type="button" class="btn btn-default" aria-label="Left Align">
                          <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>Chi tiết
                        </a>
                    </td>  
                </tr>
            <?php
        }
        else
        {
            ?>
                <tr id="trOrder<?php echo $i; ?>" name="trOrder<?php echo $i; ?>" class="danger">
                    <td><?php echo $i; ?></td>
                    <td><?php echo $row["f_Name"]; ?></td>
                    <?php
                        $str_day = $row["OrderDate"];
                        $day = strtotime($str_day);
                        $str_day = date('d-m-Y',$day); 
                     ?>
                    <td><?php echo $str_day; ?></td>
                    <td><?php echo number_format($row["Total"]); ?></td>               
                    <td colspan="3">
                            <select class="form-control" name="txtStatus<?php echo $i; ?>" id="txtStatus<?php echo $i; ?>" onchange="changeColor(<?php echo $i; ?>)" value="<?php echo $row["Status"]; ?>">
                                <option  value="<?php echo $row["Status"]; ?>"><?php echo $row["Status"]; ?></option>
                                <option  value="Đã giao">Đã giao</option>
                            </select>
                    </td>
                    <form name="frmStatus" id="frmStatus" action="" method="post">
                        <input type="hidden" name="textID" id="textID" value="<?php echo $row["OrderID"]; ?>"/>
                        <input type="hidden" name="textStatus" id="textStatus" value=""/>
                    </form>
                    <td>
                        <a href="admin.php?act=orders&view=details&id=<?php echo $row["OrderID"]; ?>" type="button" class="btn btn-default" aria-label="Left Align">
                          <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>Chi tiết
                        </a>
                    </td>  
                </tr>
            <?php
        } 
         
        $i += 1;
    }
?>

<?php
$js = <<<JS
<script type="text/javascript">
    function changeColor(i)
    {
        var name = "trOrder"+i;
        var status = "txtStatus"+i;
        var tr = document.getElementById(name);
        var x = document.getElementById(status).value;
        
        if(x == "Chưa giao")
        {
            tr.className = "danger";
        }
        if(x == "Đã giao")
        {

            tr.className = "success";
        }
        var frm = document.getElementById("frmStatus");
        frm.textStatus.value = x;
        frm.submit();
    }
</script>
JS;
?>

