<?php
	require_once './inc_func.php';

	$sql = "select count(ProID) from products where Year(now()) - Year(Day) <= 1 order by Day DESC";
	$rs = load($sql);
	$row = $rs->fetch_assoc();
?>
<div class="row">
		<div class="panel panel-blue panel-widget ">
			<div class="row no-padding">
				<div class="col-sm-3 col-lg-5 widget-left">
					<svg class="glyph stroked bag"><use xlink:href="#stroked-bag"></use></svg>
				</div>
				<div class="col-sm-9 col-lg-7 widget-right">
					<div class="large"><?php echo $row["count(ProID)"]; ?></div>
					<div class="text-muted">Sản phẩm mới</div>
				</div>
			</div>
		</div>
		<?php
			$sql2 = "select sum(view) from products";
			$rs2 = load($sql2);
			$row2 = $rs2->fetch_assoc();
		?>
		<div class="panel panel-red panel-widget">
			<div class="row no-padding">
				<div class="col-sm-3 col-lg-5 widget-left">
					<svg class="glyph stroked app-window-with-content"><use xlink:href="#stroked-app-window-with-content"></use></svg>
				</div>
				<div class="col-sm-9 col-lg-7 widget-right">
					<div class="large"><?php echo $row2["sum(view)"] ?></div>
					<div class="text-muted">Lượt xem</div>
				</div>
			</div>
		</div>
</div>