<?php
require_once './inc_func.php';
require_once './dbHelper.php';

if(isset($_POST["txtProId"])) {
    $sp = $_POST["txtProId"];
    $slg = 1;
    setCart($sp, $slg);
    print_r(getCart());
    redirect("index.php?ord=1");
}


?>
            
<?php
    if (isset($_GET["id"])) {
        $id = $_GET["id"];
        $name = $_GET["name"];
        $type = $_GET["type"];
        $cat = $_GET["cat"];
        
        $sql = "select * from products p, categories c, typeproducts t where p.CatID = c.CatID and p.ProType = t.TypeID and p.ProID = $id";
        $rs = load($sql);
        ?>
            <form id="f" action="" method="post">
                <input type="hidden" id="txtProId" name="txtProId"/>
            </form>
        <?php
        $row = $rs->fetch_assoc();
        ?>
          
          <div class="center_title_bar"><?php echo $name; ?></div>
          <div class="prod_box_big">
            <div class="top_prod_box_big"></div>
            <div class="center_prod_box_big">
              <div class="product_img_big"> 
              <a href="images/<?php echo $row["ProID"]; ?>/<?php echo $row["ProID"]; ?>.jpg" data-lightbox="<?php echo $row["ProID"]; ?>" data-title="<?php echo $row["ProName"]; ?>">
                <img src="images/<?php echo $row["ProID"]; ?>/<?php echo $row["ProID"]; ?>_mini.jpg" alt="" border="0" width="150" />
              </a>
              </div>
              <div class="details_big_box">
                <div class="product_title_big"><?php echo $row["ProName"]; ?></div>
                <div class="specifications"> Description: <span class="blue"><?php echo $row["TinyDes"]; ?>
                  <table>
                    <div id="myModal" class="modal fade" tabindex="-1">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"><?php echo $name; ?></h4>
                          </div>
                          <div class="modal-body">
                              <p><?php echo $row["FullDes"]; ?></p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </table>
                  <a  id="myDir" href="#" data-toggle="modal" data-target="#myModal">Thông số kỹ thuật</a></span><br /><br/>
                  Số lượt xem: <span class="blue"><?php echo $row["View"]; ?> lượt xem</span><br />
                  Còn: <span class="blue"><?php echo $row["Quantity"]; ?> sản phẩm</span><br />
                  <?php
                      $sql2 = "select SUM(Quantity) from orderdetails where ProID = $id" ;
                      $rs2 = load($sql2);
                      $row2 = $rs2->fetch_assoc();
                      if($row2["SUM(Quantity)"] == Null)
                      {
                          ?>
                              Đã bán: <span class="blue">0 sản phẩm</span><br />
                          <?php
                      }
                      else
                      {
                          ?>
                              Đã bán: <span class="blue"><?php echo $row2["SUM(Quantity)"]; ?> sản phẩm</span><br />
                          <?php
                      }
                   ?>
                  Xuất xứ: <span class="blue">Nhật bản</span><br />
                  Loại máy: <span class="blue"><?php echo $row["TypeName"]; ?> </span><br />
                  Nhà sản xuất: <span class="blue"><?php echo $row["CatName"]; ?></span><br />
                </div>
                <?php 
                  if(isset($_GET["price"]))
                  {
                     ?>
                        <div class="prod_price_big">Giá bán: <span class="price"><?php echo $_GET["price"]; ?> vnđ</span> <span class="reduce"><?php echo number_format($row["Price"]); ?> vnđ </span></div>
                     <?php
                  }
                  else
                  {
                     ?>
                      <div class="prod_price_big">Giá bán: <span class="price"><?php echo number_format($row["Price"]); ?> vnđ</span></div>
                    <?php
                  }
                 
                  if(isAuthenticated())
                  {
                      ?>
                          <a href="#" role="button" onclick="setProId(<?php echo $row["ProID"]; ?>)" title="header=[Đặt hàng] body=[&nbsp;] fade=[on]" class="addtocart">add to cart</a> 
                      <?php
                  } 
                ?> 
              </div>
            </div>
            <div class="bottom_prod_box_big"></div>
          </div> 
          <div class="center_title_bar">Sản phẩm cùng loại</div>
          <?php
              require_once './inc_similarPro.php'; 
          ?> 
          <div class="center_title_bar">Sản phẩm cùng nhà sản xuất</div>   
        <?php
          require_once './inc_similarPro2.php'; 
    } else {
        redirect("index.php");
    }
?>
<?php
$js = <<<JS
<script src="assets/lightbox2/js/lightbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
    function setProId(id) {
        f.txtProId.value = id;
        f.submit();
    }
</script>
JS;
?>