<?php
require_once './dbHelper.php';
require_once './inc_func.php';

if (isset($_POST["btnInsert"])) {
    $id = $_POST["txtCatID"];
    $catname = $_POST["txtCatName"];

    $sql = "select * from categories where CatID = $id";
    $rs = load($sql);
    if($rs->num_rows != 0)
    {
        ?>
          <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <span>MÃ HÃNG SẢN XUẤT ĐÃ TỒN TẠI</span>
          </div>
      <?php
    }
    else
    {
        $sql2 = "Insert into categories (CatID, CatName) values ('$id','$catname')";
        $n = save($sql2,1);

        redirect("admin.php?act=categories");
    }    
}
?>

<form id="productForm" method="post" class="form-horizontal" enctype="multipart/form-data">
    <div class="form-group">
        <label class="col-xs-4 control-label">Tên hãng sản xuất</label>
        <div class="col-xs-5">
            <input type="text" class="form-control" name="txtCatName" id="txtCatName" value="" />
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-4 control-label">Mã hãng sản xuất</label>
        <div class="col-xs-5">
            <input type="text" class="form-control" name="txtCatID" id="txtCatID" value="" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-4 col-xs-offset-4">
            <button type="submit" class="btn btn-default" name="btnInsert" id="btnInsert">Thêm</button>
        </div>
    </div>
</form>

<?php
$js = <<<JS
<script src="js/formValidation.min.js"></script>
<script src="js/framework/bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#productForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                'txtCatName': {
                    validators: {
                        notEmpty: {
                            message: 'Không được để trống tên hãng sản xuất'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Tên hãng sản xuất phải dài từ 3 đến 30 ký tự'
                        }
                    }
                },
                txtCatID: {
                    validators: {
                        notEmpty: {
                            message: 'Không được để trống mã hãng sản xuất'
                        },
                        numeric: {
                            message: 'Mã nhà sản xuất phải là số'
                        }
                    }
                }
            }
        })
        .end()
});
</script>
JS;
?>

