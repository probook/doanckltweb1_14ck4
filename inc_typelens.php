<?php

require_once './dbHelper.php';

$sql = "select * from typeproducts";
$rs = load($sql);

while ($row = $rs->fetch_assoc()) {
    $id = $row["TypeID"];
    $name = $row["TypeName"];
    ?>
            <li role="separator" class="divider"></li>
            <li><a href="listProducts.php?act=type&id=<?php echo $id; ?>&name=<?php echo $name; ?>&page=1"><?php echo $name; ?></a></li>
    <?php
}
?>