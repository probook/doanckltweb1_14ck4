<?php require_once './inc_func.php';?>
<nav class="navbar navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">
                <i class="fa fa-home"></i>
                14CK4
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <?php
                if(isAuthenticated())
                {
                    $u = $_SESSION["auth_user"];
                    if($u["f_Permission"] == 1 || $u["f_Permission"] == 2)
                    {

                    }
                    else
                    {
                        ?>
                            <ul class="nav navbar-nav">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Nhà sản xuất <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <?php include_once "./inc_productors.php"; ?>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Loại máy ảnh <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <?php include_once "./inc_typelens.php"; ?>
                                    </ul>
                                </li>
                            </ul>
                        <?php
                    }
                }
                else
                {
                    ?>
                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Nhà sản xuất <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <?php include_once "./inc_productors.php"; ?>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Loại máy ảnh <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <?php include_once "./inc_typelens.php"; ?>
                                </ul>
                            </li>
                        </ul>
                    <?php
                } 
             ?>
            
            <form action="search.php" class="navbar-form navbar-left">
                <div class="form-group">
                    <input id="item" name="item" type="text" class="form-control" placeholder="Tìm: Sản phẩm, Giá, Loại ống kính, Nhà sản xuất,...">
                </div>
                <?php
                    if(isset($_GET["item"]))
                    {
                        $name = $_GET["item"];
                    }
                ?>
                <button type="submit" class="btn btn-danger" title="tìm kiếm">
                    <i class="fa fa-search"></i>
                </button>
            </form>
            <ul class="nav navbar-nav navbar-right">

                <?php
                if (isAuthenticated() == false) {
                    ?>
                    <li><a href="index.php?act=login">Đăng nhập</a></li>
                    <li><a href="index.php?act=register">Đăng ký</a></li>
                    <?php
                } else {
                        $u = $_SESSION["auth_user"];
                        if($u["f_Permission"] == 1 || $u["f_Permission"] == 2)
                        {
                            ?>
                                <li>
                                    <a href="index.php?act=admin">DashBoard</a>
                                </li>
                            <?php
                        }
                        else
                        {
                            ?>
                                <li>
                                    <a href="index.php?act=cart">Giỏ hàng có <?php echo cart_sum_items(); ?> sản phẩm!</a>
                                </li>
                            <?php
                        }
                    ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION["auth_user"]["f_Name"]; ?> <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <?php
                                $u = $_SESSION["auth_user"];
                                if($u["f_Permission"] == 1 || $u["f_Permission"] == 2)
                                {
                                    ?>
                                        <li>
                                            <a href="index.php?act=profile">
                                                <i class="fa fa-user"></i>
                                                Xem thông tin cá nhân
                                            </a>
                                        </li>
                                    <?php
                                } 
                                else
                                {
                                    ?>
                                        <li>
                                            <a href="index.php?act=profile&person=<?php echo $u["f_ID"] ?>">
                                                <i class="fa fa-user"></i>
                                                Xem thông tin cá nhân
                                            </a>
                                        </li>
                                    <?php
                                }
                             ?>
                            
                            <li role="separator" class="divider"></li>
                            <li>
                                <a href="logout.php">
                                    <i class="fa fa-sign-out"></i>
                                    Thoát
                                </a>
                            </li>
                        </ul>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
