<?php
require_once './inc_func.php';

if (isAuthenticated() == false) {
    redirect("index.php?act=login&register=1");
}
 $u = $_SESSION["auth_user"];
?>


<div class="col-md-12" >
  <div class="panel panel-info">
    <div class="panel-heading">
      <h3 class="panel-title"><?php echo $u["f_Name"] ?></h3>
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="images/avatar.png" class="img-circle img-responsive"> </div>
        
        <!--<div class="col-xs-10 col-sm-10 hidden-md hidden-lg"> <br>
          <dl>
            <dt>DEPARTMENT:</dt>
            <dd>Administrator</dd>
            <dt>HIRE DATE</dt>
            <dd>11/12/2013</dd>
            <dt>DATE OF BIRTH</dt>
               <dd>11/12/2013</dd>
            <dt>GENDER</dt>
            <dd>Male</dd>
          </dl>
        </div>-->
        <div class=" col-md-9 col-lg-9 "> 
          <table class="table table-user-information">
            <tbody>
              <tr>
                <td>Họ tên:</td>
                <td><?php echo $u["f_Name"] ?></td>
              </tr>
              <tr>
                <td>Tên đăng nhập:</td>
                <td><?php echo $u["f_Username"] ?></td>
              </tr>
              <tr>
                <td>Mật khẩu:</td>
                <td><?php echo $u["f_Password"] ?></td>
              </tr>
              <tr>
                <td>Ngày sinh:</td>
                <td><?php 
                        $str_dob = $u["f_DOB"];
                        $dob = strtotime($str_dob);
                        $str_dob = date('d-m-Y', $dob);
                        $str_dob = str_replace('-','/',$str_dob);
                        echo $str_dob;
                    ?>       
                </td>
              </tr>
              </tr>
              <tr>
                <td>Email:</td>
                <td><a href="<?php echo $u["f_Email"] ?>"><?php echo $u["f_Email"] ?></a></td>  
              </tr>
              <tr>
                <td>Loại người dùng:</td>
                <?php
                    $per = $u["f_Permission"];
                    switch ($per) {
                        case 0:
                             ?><td>Khách hàng</td><?php
                             break;
                        case 1:
                        case 2:
                             ?><td>Quản trị viên</td><?php
                             break;
                         default:
                             break;
                     } 
                 ?>
              </tr>   
            </tbody>
          </table>  
        </div>
      </div>
    </div>
          <?php
              if($u["f_Permission"] == 1 || $u["f_Permission"] == 2)
              {
                ?>
                    <div class="panel-footer">
                        <a href="index.php?act=edit" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a>
                    </div>
                <?php
              }
              else
              {
                ?>
                    <div class="panel-footer">
                        <a href="index.php?act=edit&person=<?php echo $u["f_ID"] ?>" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a>
                    </div>
                <?php
              } 
           ?>
  </div>
</div>