<?php
require_once './inc_func.php';
require_once './dbHelper.php';

?>

<?php
    $sql = "select * from categories";
    $rs = load($sql);
    $i = 1;
    while ($row = $rs->fetch_assoc()) {
        ?>
            <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $row["CatName"]; ?></td>
                <td><?php echo $row["CatID"]; ?></td>
                <td colspan="2">
                    <a href="admin.php?act=delete&catid=<?php echo $row["CatID"]; ?>" class="btn btn-default" aria-label="Left Align" title="Xóa nhà sản xuất">
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                    </a>
                    <a href="admin.php?act=editcat&catid=<?php echo $row["CatID"] ?>&catname=<?php echo $row["CatName"]; ?>" class="btn btn-default" aria-label="Left Align" title="Chỉnh sửa">
                        <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                    </a>
                </td>  
            </tr>
        <?php
        $i += 1;
    }
?>