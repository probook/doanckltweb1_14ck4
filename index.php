<?php session_start(); 
      ob_start();?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Camera Store</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
	<link href="assets/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
  <link href="css/formValidation.min.css" rel="stylesheet" type="text/css"/>
  <link href="css/profile.css" rel="stylesheet" type="text/css"/>
  <link href="css/editprofile.css" rel="stylesheet" type="text/css"/>
  <link href="assets/font-awesome-4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
  <link href="assets/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>
  <link href="assets/lightbox2/css/lightbox.min.css" rel="stylesheet" type="text/css"/>
  <link href="css/hoverDropDown.css" rel="stylesheet" type="text/css"/>
  <link href="css/viewright.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" type="text/css" href="style.css" />
  <style type="text/css" media="screen">
    .center_prod_box{
      width: 173px;
      height: 173px;
      text-align:center;
    }
    .center_title_bar{
      padding:0 0 0 50px;
    }
    .right_content{
      float: left;
      margin-left: 40px;
    }
    .title_box{
      color:#fff;
    }
    #header{
      width:1000px;
      height:353px;
      background: url(images/header_bg.jpg) repeat-y center;
      background-size: 1000px 353px;
      margin:auto;
    }
    body
    {
      padding-top: 55px;
      background:url(images/bg.jpg) no-repeat #fff center top;
      color:#fff;
    }
    .cart_details{
      color: #000000;
    }
    .title 
    {
      font-family: 'Open Sans';
      font-size: 16pt;
      padding-top: 10px;
      padding-bottom: 10px;
      border-bottom: 1px solid #cccccc;
      margin-bottom: 35px;
      color: black;
    }
    
    .datepicker{
      width: 200px;
    }
    .datepicker td,
    .datepicker th {
      -webkit-text-fill-color: black;
    }
    .datepicker td.old,
    .datepicker td.new {
      -webkit-text-fill-color: #868686;
    }
    table td,
          th{
      -webkit-text-fill-color: black;
    }
    .div-square {
        padding:5px;
        border:3px double #e1e1e1;
        -webkit-border-radius:8px;
       -moz-border-radius:8px;
        border-radius:8px;
        margin:5px;
        text-align: center;
        height: 120px;
    }
    .div-square> a,.div-square> a:hover {
        color: #3AC7AE;
        text-decoration: none;
    }
    .navbar{
      margin-top: 0px;
    }
  </style>
	<!--[if IE 6]>
	<link rel="stylesheet" type="text/css" href="iecss.css" />
	<![endif]-->
  <script type="text/javascript" src="js/boxOver.js"></script>
  <script type="text/javascript" src="js/hoverDropDown.js"></script>
  <script type="text/javascript" src="js/lumino.glyphs.js"></script>
  <style type="text/css">
    /* Adjust feedback icon position */
    #productForm .selectContainer .form-control-feedback,
    #productForm .inputGroupContainer .form-control-feedback {
        right: -15px;
    }
  </style>
</head>
<body>
	<div id="main_container">
  <div class="top_bar">
     <?php include_once "./inc_top.php"; ?>
  </div>
  <?php
      if(isAuthenticated())
      {
          $u = $_SESSION["auth_user"];
          if($u["f_Permission"] == 1 || $u["f_Permission"] == 2)
          {

          }
          else
          {
              ?>
                <div id="header">
                  <div class="oferte_content">
                  <div class='carousel slide' data-ride='carousel' id='carousel-example-generic'>
                  <!-- Indicators -->
                  <ol class='carousel-indicators'>
                    <li class='active' data-slide-to='0' data-target='#carousel-example-generic'/>
                    <li data-slide-to='1' data-target='#carousel-example-generic'/>
                    <li data-slide-to='2' data-target='#carousel-example-generic'/>
                    <li data-slide-to='3' data-target='#carousel-example-generic'/>
                  </ol>

                  <!-- Wrapper for slides -->
                  <div class='carousel-inner' role='listbox'>
                    <div class='item active'>
                      <img alt='' src='images/header_top01.jpg'/>
                      <div class='carousel-caption'>
                      </div>
                    </div>
                    <div class='item'>
                      <img alt='' src='images/header_top02.jpg'/>
                      <div class='carousel-caption'>
                      </div>
                    </div>
                    <div class='item'>
                      <img alt='' src='images/header_top03.jpg'/>
                      <div class='carousel-caption'>
                      </div>
                    </div>
                    <div class='item'>
                      <img alt='' src='images/header_top04.jpg'/>
                      <div class='carousel-caption'>
                      </div>
                    </div>
                  </div>

                  <!-- Controls -->
                  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
              <?php
          }
      }
      else
      {
          ?>
            <div id="header">
              <div class="oferte_content">
              <div class='carousel slide' data-ride='carousel' id='carousel-example-generic'>
              <!-- Indicators -->
              <ol class='carousel-indicators'>
                <li class='active' data-slide-to='0' data-target='#carousel-example-generic'/>
                <li data-slide-to='1' data-target='#carousel-example-generic'/>
                <li data-slide-to='2' data-target='#carousel-example-generic'/>
                <li data-slide-to='3' data-target='#carousel-example-generic'/>
              </ol>

              <!-- Wrapper for slides -->
              <div class='carousel-inner' role='listbox'>
                <div class='item active'>
                  <img alt='' src='images/header_top01.jpg'/>
                  <div class='carousel-caption'>
                  </div>
                </div>
                <div class='item'>
                  <img alt='' src='images/header_top02.jpg'/>
                  <div class='carousel-caption'>
                  </div>
                </div>
                <div class='item'>
                  <img alt='' src='images/header_top03.jpg'/>
                  <div class='carousel-caption'>
                  </div>
                </div>
                <div class='item'>
                  <img alt='' src='images/header_top04.jpg'/>
                  <div class='carousel-caption'>
                  </div>
                </div>
              </div>

              <!-- Controls -->
              <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
          <?php
      } 
   ?>
  
  <hr>
  <div id="main_content">
    <div class="left_content">
      <?php
          if(isset($_GET["person"]))
          {
            $u = $_SESSION["auth_user"];
            ?>                      
                      <div class="profile-sidebar">
                        <!-- SIDEBAR USERPIC -->
                        <div class="profile-userpic">
                            <img src="images/avatar.png" class="img-responsive" alt="">           
                        </div>
                        <!-- END SIDEBAR USERPIC -->
                        <!-- SIDEBAR USER TITLE -->
                        <div class="profile-usertitle">
                          <div class="profile-usertitle-name">
                            <span class="hidden-xs"><?php echo $u["f_Name"] ?></span>
                          </div>
                          <div class="profile-usertitle-job">
                            <span class="hidden-xs">Khách hàng</span>
                          </div>
                        </div>
                        <!-- END SIDEBAR USER TITLE -->
                        <!-- SIDEBAR MENU -->
                        <div class="profile-usermenu">
                          <ul class="nav">
                            <li>
                              <a href="index.php?act=profile&person=<?php echo $u["f_ID"] ?>">
                              <i class="glyphicon glyphicon-home"></i>
                              <span class="hidden-xs">Thông tin cá nhân<span> </a>
                            </li>
                            <br>
                            <li>
                              <a href="index.php?act=edit">
                              <i class="glyphicon glyphicon-user"></i>
                              <span class="hidden-xs">Cập nhật thông tin<span> </a>
                            </li>
                            <br>
                            <li>
                              <a href="index.php?act=cart">
                              <i class="glyphicon glyphicon-shopping-cart"></i>
                              <span class="hidden-xs">Xem giỏ hàng<span> </a>         
                            </li>
                                        
                          </ul>
                        </div>
                        <!-- END MENU -->
                      </div>
            <?php
          } 
          else
          {
            if(isAuthenticated())
            {
              if($u["f_Permission"] == 1 || $u["f_Permission"] == 2)
              {
                    $u = $_SESSION["auth_user"];
                    ?>                      
                        <div class="profile-sidebar">
                          <!-- SIDEBAR USERPIC -->
                          <div class="profile-userpic">
                              <img src="images/avatar.png" class="img-responsive" alt="">           
                          </div>
                          <!-- END SIDEBAR USERPIC -->
                          <!-- SIDEBAR USER TITLE -->
                          <div class="profile-usertitle">
                            <div class="profile-usertitle-name">
                              <span class="hidden-xs"><?php echo $u["f_Name"] ?></span>
                            </div>
                            <div class="profile-usertitle-job">
                              <span class="hidden-xs">Quản trị viên</span>
                            </div>
                          </div>
                          <!-- END SIDEBAR USER TITLE -->
                          <!-- SIDEBAR MENU -->
                          <div class="profile-usermenu">
                            <ul class="nav">
                              <li>
                                <a href="index.php?act=admin&Dashboard=access">
                                <i class="glyphicon glyphicon-credit-card"></i>
                                <span class="hidden-xs">DashBoard<span> </a>         
                              </li>
                              <?php
                                  if(isset($_GET["Dashboard"]))
                                  {
                                      ?>
                                      <li>
                                        <a href="admin.php?act=user">
                                        <i class="fa fa-users"></i>
                                        <span class="hidden-xs"><b>Người dùng</b><span> </a>         
                                      </li>
                                      <li>
                                        <a href="admin.php?act=products">
                                        <i class="fa fa-archive"></i>
                                        <span class="hidden-xs"><b>Sản phẩm</b><span> </a>         
                                      </li>
                                      <li>
                                        <a href="admin.php?act=typeproducts">
                                        <i class="fa fa-list-ol"></i>
                                        <span class="hidden-xs"><b>Loại sản phẩm</b><span> </a>         
                                      </li>
                                      <li>
                                        <a href="admin.php?act=categories">
                                        <i class="fa fa-building-o"></i>
                                        <span class="hidden-xs"><b>Nhà sản xuất</b><span> </a>         
                                      </li>
                                      <li>
                                        <a href="admin.php?act=orders">
                                        <i class="fa fa-clipboard"></i>
                                        <span class="hidden-xs"><b>Đơn hàng</b><span> </a>         
                                      </li>
                                      <?php
                                  } 
                               ?>
                              <br>
                              <li>
                                <a href="index.php?act=profile">
                                <i class="glyphicon glyphicon-home"></i>
                                <span class="hidden-xs">Thông tin cá nhân<span> </a>
                              </li>
                              <br>
                              <li>
                                <a href="index.php?act=edit">
                                <i class="glyphicon glyphicon-user"></i>
                                <span class="hidden-xs">Cập nhật thông tin<span> </a>
                              </li>        
                            </ul>
                          </div>
                          <!-- END MENU -->
                        </div>
                    <?php
              }
              else
              {
                ?>
                    <div class="title_box">Nhà sản xuất</div>
                      <ul class="left_menu">
                        <?php include_once "./inc_left.php"; ?>
                      </ul>
                      <div class="title_box">Loại máy ảnh</div>
                      <ul class="left_menu">
                        <?php include_once "./inc_midleft.php"; ?>
                      </ul>
                      <div class="title_box">Sản phẩm đặc biệt</div>
                      <div class="border_box">
                        <?php include_once "./inc_botLeft.php"; ?>
                    </div>
                <?php
              }
            }
            else
            {
              ?>
                  <div class="title_box">Nhà sản xuất</div>
                    <ul class="left_menu">
                      <?php include_once "./inc_left.php"; ?>
                    </ul>
                    <div class="title_box">Loại máy ảnh</div>
                    <ul class="left_menu">
                      <?php include_once "./inc_midleft.php"; ?>
                    </ul>
                    <div class="title_box">Sản phẩm đặc biệt</div>
                    <div class="border_box">
                      <?php include_once "./inc_botLeft.php"; ?>
                  </div>
              <?php
            }
          }
       ?>
    </div>
    <!-- end of left content -->
    <div class="center_content">
      <?php
          if(isset($_GET["ord"]))
          {
            ?>
                <div float="right" class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <span>Bạn đã đặt hàng thành công sản phẩm này</span>
                </div>
            <?php
          } 
          if(isset($_GET["pay"]))
          {
              ?>
                  <div float="right" class="alert alert-success alert-dismissible" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <span>Thanh toán thành công đơn hàng</span>
                  </div>
               <?php
          }
          if (isset($_GET["act"])) {
              $act = $_GET["act"];
              switch ($act) {
                  case "login":
                      include_once './inc_login.php';
                      break;
                  case "register":
                      include_once './inc_register.php';
                      break;
                  case "profile":
                      include_once './inc_profile.php';
                      break;
                  case "edit":
                      include_once './inc_editProfile.php';
                      break;
                  case "cart":
                      include_once './inc_cart.php';
                      break;
                  case "admin":
                      include_once './inc_dashBoard.php';
                      break;

                  default:
                      ?>
                      <div class="center_title_bar">Sản phẩm mới</div>
                      <?php include_once "./inc_products.php"; ?>
                      <div class="center_title_bar">Sản phẩm bán chạy</div>
                      <?php include_once "./inc_products2.php"; ?>
                      <div class="center_title_bar">Sản phẩm nhiều người xem</div>
                      <?php include_once "./inc_products3.php";
                      break;
              }
          } else {
              ?>
              <div class="center_title_bar">Sản phẩm mới</div>
              <?php include_once "./inc_products.php"; ?>
              <div class="center_title_bar">Sản phẩm bán chạy</div>
              <?php include_once "./inc_products2.php"; ?>
              <div class="center_title_bar">Sản phẩm nhiều người xem</div>
              <?php include_once "./inc_products3.php";
          }
      ?>
      
    </div>
    <!-- end of center content -->
    <div class="right_content">
      <?php 
        require_once './inc_func.php'; 
        if (isAuthenticated())
        {
          if($u["f_Permission"] == 1 || $u["f_Permission"] == 2)
          {
              include_once "./inc_adview.php";
          }
          else
          {
              ?>
                <div class="shopping_cart">
                  <div class="cart_title">Giỏ hàng</div>
                  <div class="cart_details"> Có <?php echo cart_sum_items(); ?> sản phẩm <br />
                    <?php
                      $total = 0;
                      foreach ($_SESSION["cart"] as $id => $quantity) {
                          $sql = "select * from products where ProID=$id";
                          $rs = load($sql);
                          $row = $rs->fetch_assoc();
                          $total += $row["Price"] * $quantity;
                      }
                      ?>
                    <span class="border_cart"></span> Tổng tiền: <span class="price"><?php echo number_format($total); ?> vnđ</span> </div>
                  <div class="cart_icon"><a href="index.php?act=cart" title="header=[Xem giỏ hàng] body=[&nbsp;] fade=[on]"><img src="images/shoppingcart.png" alt="" width="48" height="48" border="0" /></a></div>
                </div>
                <div class="title_box">Sản phẩm mới nhất</div>
                    <?php include_once "./inc_newProducts.php";?>
                </div>
              <?php
          }
        }
        else
        {
          ?>
              <div class="title_box">Sản phẩm mới nhất</div>
                  <?php include_once "./inc_newProducts.php";?>
              </div>
          <?php
        }
      ?>
    <!-- end of right content -->
  </div>
  <!-- end of main content -->
</div>
  <script src="assets/jquery-3.1.1.min.js" type="text/javascript"></script>
  <script src="assets/bootstrap-3.3.7-dist/js/bootstrap.min.js" type="text/javascript"></script>
  <?php
    if (isset($js)) {
        echo $js;
    }
  ?>
</body>
</html>