<?php
require_once './dbHelper.php';
require_once './inc_func.php';

if (isAuthenticated() == false) {
    redirect("index.php?act=login&register=1");
}
if(isset($_GET["id"]))
{
    $u["f_ID"] = $_GET["id"];
    $u["f_Username"] = $_GET["username"];
    $u["f_Name"] = $_GET["name"];
    $u["f_Email"] =  $_GET["email"];
    $u["f_DOB"] = $_GET["dob"];

    $dob = strtotime($u["f_DOB"]); //Y-m-d
    $u["f_DOB"] = date('d-m-Y', $dob);
    $u["f_DOB"] = str_replace('-', '/', $u["f_DOB"]);
}
else
{
    $u = $_SESSION["auth_user"];
}

if (isset($_POST["btnUpdate"])) {
    $id = $_POST["txtID"];
    $uid = $_POST["uname"];
    $uname = $_POST["fname"];
    $str_dob = $_POST["dob"];
    $email = $_POST["email"];

    $permission = $_POST["txtPermission"];
    $str_dob = str_replace('/', '-', $str_dob);
    $dob = strtotime($str_dob); //d-m-Y
    $str_dob = date('Y-m-d H:i:s', $dob);

    $sql2 = "Update users set f_Name = '$uname', f_DOB = '$str_dob', f_Email = '$email', f_Permission = '$permission', f_Username = '$uid' where f_ID = '$id'";
    $id = save($sql2,1);

    redirect("admin.php?act=user");
    }
?>




    <div class="col-md-12 col-xs-offset-0 order-content">
          
      <div class="form_main col-md-4 col-sm-5 col-xs-7">
                <h4 class="heading"><strong>Chỉnh sửa  </strong> Thông tin cá nhân <span></span></h4>
                <div class="form">
                <form action="" method="post" id="frmUpdate" name="frmUpdate" onsubmit="return validate()">
                <input type="hidden" name="txtID" id="txtID" value="<?php echo $u["f_ID"]; ?>">
                    <label>TÊN ĐĂNG NHẬP</label><br>
                    <input type="text" required placeholder="<?php echo $u["f_Username"]; ?>" value="<?php echo $u["f_Username"]; ?>" id="uname" name="uname" class="txt"><br><br>
                    <label>HỌ TÊN</label><br>
                    <input type="text" required placeholder="Họ tên" value="<?php echo $u["f_Name"]; ?>" onblur="bgcolor(this,1)" id="fname" name="fname" class="txt"><br><br>
                    <label>NGÀY SINH</label><br>
                    <input type="text" required placeholder="Ngày sinh" value="<?php echo $u["f_DOB"]; ?>" id="dob" name="dob"><br><br> 
                    <label>ĐỊA CHỈ EMAIL</label><br>
                    <input type="text" required placeholder="Email" onblur="bgcolor(this,1)" value="<?php echo $u["f_Email"]; ?>" id="email" name="email" class="txt"><br><br>
                    <label>LOẠI NGƯỜI DÙNG</label>
                    <select class="form-control" name="txtPermission" id="txtPermission">
                        <option value="">Chọn một loại</option>
                        <option value="2">Quản trị viên</option>
                        <option value="0">Khách hàng</option>
                    </select>
                    <br>
                    <button type="submit" name="btnUpdate" id="btnUpdate" class="btn btn-default">Update</button>
                </form>
            </div>
      </div>            
  </div>

<?php
$js = <<<JS
<script src="js/formValidation.min.js"></script>
<script src="js/framework/bootstrap.min.js"></script>
<script src="assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script type="text/javascript">
    function validate() 
    {
        var fullname= document.getElementById("fname");
        var dob = document.getElementById("dob");
        var email =document.getElementById("email");

          if(allLetter(fullname))
         {
              return true;
         }
         else
         {
              bgcolor(fullname,2);
         }
        return false;
    } 

    function pwd_validation(pwd,mx,my)  
    {  
        var pwd_len = pwd.value.length;  
        if (pwd_len == 0 ||pwd_len > my || pwd_len < mx)  
        {  
            alert("Mật khẩu không được để trống / độ dài mật khẩu phải từ "+mx+" đến "+my+ " ký tự ");  
            return false;  
        }  
        return true;  
    } 

    function allLetter(uname)  
    {   
        var letters = /^[A-Za-z ][A-Za-z0-9!ÀáÂãÈÉÊÌÍÒóÔÕÙÚÝàáâãèéêìíòóôõùúýĂăĐđĨĩŨũƠơƯưẠạẢảẤấẦầẨẩẪẫẬậẹẸặẶẵẴẳẲằẰắẴẺẻẼẽẾếỀềỂểỄễỆệỈỉỊịỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞởỠỡỢợỤụỦủỨứỪừỬửỮữỰựỲỳỴỵỶỷỸỹ ]+$/;
        if(uname.value.match(letters))
        {
            return true; 
        }
        else
        {
            alert("Họ tên không được chứa ký tự đặc biệt");
            return false;
        }    
    }

    function bgcolor(a,flag)
    {
        switch (flag) {
            case 1:
                a.style.background = 'white';
                break;
            case 2:
                a.style.background = '#E87070';
                break;
        }
        
    }
</script>
<script type="text/javascript">
$(function () {
      $('.datepicker').datepicker({ format: "dd/mm/yyyy" }).on('changeDate', function (ev) {
        $(this).datepicker('hide');
      });
    });
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#frmUpdate')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                dob: {
                    validators: {
                        notEmpty: {
                            message: 'Không được để trống ngày sinh'
                        },
                        date:{
                            format: 'DD/MM/YYYY',
                            max: '30/12/2002',
                            message: 'Tuổi không hợp lệ'
                        },
                    }
                },
            }
        })
 
        .end()
});
</script>
JS;
?>

