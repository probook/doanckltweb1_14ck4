<?php
require_once './dbHelper.php';
require_once './inc_func.php';

if (isset($_POST["btnLogin"])) {
    $uid = $_POST["txtUID"];
    $pwd = $_POST["txtPWD"];
    $enc_pwd = md5($pwd);

    $sql = "select * from users where f_Username = '$uid' and f_Password = '$enc_pwd'";
    $rs = load($sql);
    if ($rs->num_rows == 0) {
        $login_fail = 1;
    } 
    else {

        $_SESSION["auth"] = 1;

        $row = $rs->fetch_assoc();
        $u = array();
        $u["f_Username"] = $row["f_Username"];
        $u["f_Password"] = $pwd;
        $u["f_ID"] = $row["f_ID"];
        $u["f_Name"] = $row["f_Name"];
        $u["f_Email"] = $row["f_Email"];
        $u["f_DOB"] = $row["f_DOB"];
        $u["f_Permission"] = $row["f_Permission"];
        //print_r($u);
        $_SESSION["auth_user"] = $u;

        // $_COOKIE["auth_user_id"] = $row["f_ID"];

        $remember = isset($_POST["chkRememberMe"]) ? true : false;
        if ($remember) {
            $expire = time() + 7 * 24 * 60 * 60;
            setcookie("auth_user_id", $row["f_ID"], $expire);
        }

        //$_SESSION["auth_username"] = $row["f_Username"];
        //$_SESSION["auth_id"] = $row["f_ID"];

        if(isset($_GET["register"]))
        {
            $re = isset($_GET["register"]);
            if($re == 1)
            {
                redirect("index.php?act=profile");
            }
        }
        if($u["f_Permission"] == 1 || $u["f_Permission"] == 2)
        {
            redirect("index.php?act=admin");
        }
        else
        {
            redirect("index.php");
        }
        
    }
}

if (isset($_GET["register"])) {
    if(isset($_GET["register"]) == 1)
    {
        ?>
            <div float="right" class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <span>Bạn đã tạo tài khoản thành công hãy đăng nhập bằng tài khoản bạn vừa tạo</span>
            </div>
        <?php
    }
}

if (isset($_GET["update"])) {
    if(isset($_GET["update"]) == 1)
    {
        ?>
            <div float="right" class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <span>Bạn đã cập nhật tài khoản thành công hãy đăng nhập lại</span>
            </div>
        <?php
    }
}

?>

<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Đăng nhập</h3>
        </div>
        <div class="panel-body">
            <form  class="form-horizontal" action="" method="post" id="loginForm">

                <?php
                if (isset($login_fail) && $login_fail == 1) {
                    ?>
                    <div class="alert alert-warning alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <span>Đăng nhập thất bại</span>
                    </div>
                    <?php
                }
                ?>

                <div class="row">
                    <div class="col-md-10 col-md-offset-1 title">
                        Thông tin đăng nhập
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="txtUID" name="txtUID" placeholder="Tên đăng nhập">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="password" class="form-control" id="txtPWD" name="txtPWD" placeholder="Mật khẩu">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-7">
                                <label style="font-weight: normal">
                                    <input type="checkbox" name="chkRememberMe" /> Ghi nhớ
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <button type="submit" class="btn btn-primary pull-right" name="btnLogin" id="btnLogin">
                                    <i class="fa fa-check"></i> Đăng nhập
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>