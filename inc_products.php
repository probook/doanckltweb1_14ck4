<?php
require_once './inc_func.php';
require_once './dbHelper.php';

if(isset($_POST["txtProId"])) {
    $sp = $_POST["txtProId"];
    $slg = 1;
    setCart($sp, $slg);
    print_r(getCart());
    redirect("index.php");
    ob_end_flush();
}

?>

<?php
    $sql = "select * from products order by Day DESC";
    $rs = load($sql);
    $count = 1;
    ?>
        <form id="f" action="" method="post">
            <input type="hidden" id="txtProId" name="txtProId" />
        </form>
    <?php
        while ($row = $rs->fetch_assoc()) {
            if($count <= 10)
            {
                ?>
                <div class="prod_box">
                <div class="center_prod_box">
                  <div class="product_title"><a href="listProducts.php?act=details&id=<?php echo $row["ProID"]; ?>&name=<?php echo $row["ProName"]; ?>&type=<?php echo $row["ProType"]; ?>&cat=<?php echo $row["CatID"]; ?>"><?php echo $row["ProName"]; ?></a><br/></div>
                  <div class="product_img"><a href="listProducts.php?act=details&id=<?php echo $row["ProID"]; ?>&name=<?php echo $row["ProName"]; ?>&type=<?php echo $row["ProType"]; ?>&cat=<?php echo $row["CatID"]; ?>"><img src="images/<?php echo $row["ProID"];?>/<?php echo $row["ProID"];?>_mini.jpg" alt="" border="0" width="100" /></a></div>
                  <div class="prod_price"><span class="price"><?php echo number_format($row["Price"]); ?> vnđ</span></div>
                </div>
                <div class="bottom_prod_box"></div>
                <div class="prod_details_tab"> 
                <?php
                    if(isAuthenticated())
                    {
                        ?>
                            <a href="#" role="button" onclick="setProId(<?php echo $row["ProID"]; ?>)" title="header=[Đặt hàng] body=[&nbsp;] fade=[on]"><img src="images/cart.gif" alt="" border="0" class="left_bt" /></a>
                        <?php
                    } 
                ?>
                <a href="listProducts.php?act=details&id=<?php echo $row["ProID"]; ?>&name=<?php echo $row["ProName"]; ?>&type=<?php echo $row["ProType"]; ?>&cat=<?php echo $row["CatID"]; ?>" class="prod_details">details</a> </div>
            </div>
                <?php
            }
            $count+=1;
        }
        ?>
    <?php
?>

<?php
$js = <<<JS
<script src="assets/lightbox2/js/lightbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
    function setProId(id) {
        f.txtProId.value = id;
        f.submit();
    }
</script>
JS;
?>