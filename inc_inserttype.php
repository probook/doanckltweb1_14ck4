<?php
require_once './dbHelper.php';
require_once './inc_func.php';

if (isset($_POST["btnInsert"])) {
    $id = $_POST["txtTypeID"];
    $typename = $_POST["txtTypeName"];

    $sql = "select * from typeproducts where TypeID = $id";
    $rs = load($sql);
    if($rs->num_rows != 0)
    {
        ?>
          <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <span>MÃ SẢN PHẨM ĐÃ TỒN TẠI</span>
          </div>
      <?php
    }
    else
    {
        $sql2 = "Insert into typeproducts (TypeID, TypeName) values ('$id','$typename')";
        $n = save($sql2,1);

        redirect("admin.php?act=typeproducts");
    }    
}
?>

<form id="productForm" method="post" class="form-horizontal" enctype="multipart/form-data">
    <div class="form-group">
        <label class="col-xs-4 control-label">Tên loại sản phẩm</label>
        <div class="col-xs-5">
            <input type="text" class="form-control" name="txtTypeName" id="txtTypeName" value="" />
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-4 control-label">Mã loại</label>
        <div class="col-xs-5">
            <input type="text" class="form-control" name="txtTypeID" id="txtTypeID" value="" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-4 col-xs-offset-4">
            <button type="submit" class="btn btn-default" name="btnInsert" id="btnInsert">Thêm</button>
        </div>
    </div>
</form>

<?php
$js = <<<JS
<script src="js/formValidation.min.js"></script>
<script src="js/framework/bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#productForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                'txtTypeName': {
                    validators: {
                        notEmpty: {
                            message: 'Không được để trống tên loại sản phẩm'
                        },
                        stringLength: {
                            min: 6,
                            max: 50,
                            message: 'Tên loại sản phẩm phải dài từ 6 đến 50 ký tự'
                        }
                    }
                },
                txtTypeID: {
                    validators: {
                        notEmpty: {
                            message: 'Không được để trống mã sản phẩm'
                        },
                        numeric: {
                            message: 'Mã sản phẩm phải là số'
                        }
                    }
                }
            }
        })
        .end()
});
</script>
JS;
?>

