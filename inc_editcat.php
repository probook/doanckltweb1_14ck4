<?php
require_once './dbHelper.php';
require_once './inc_func.php';

if(isset($_GET["catid"]))
{
    $id = $_GET["catid"];
    $sql = "select * from categories where CatID = $id";
    $rs = load($sql);
    $row = $rs->fetch_assoc();
    $u["CatName"] = $row["CatName"];
}

if (isset($_POST["btnUpdate"])) {
    $id = $_POST["txtCatID"];
    $catname = $_POST["txtCatName"];

    $sql = "Update categories set TypeName = '$catname' where CatID = '$id'";
    $n = save($sql,1);

    redirect("admin.php?act=categories");
}
?>

<form id="typeForm" method="post" class="form-horizontal">
    <input type="hidden" class="form-control" name="txtCatID" id="txtCatID" value="<?php echo $id; ?>"/>
    <div class="form-group">
        <label class="col-xs-4 control-label">Tên hãng sản xuất</label>
        <div class="col-xs-5">
            <input type="text" class="form-control" name="txtCateName" id="txtCatName" value="<?php echo $u["CatName"]; ?>" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-4 col-xs-offset-4">
            <button type="submit" class="btn btn-default" name="btnUpdate" id="btnUpdate">Update</button>
        </div>
    </div>
</form>

<?php
$js = <<<JS
<script src="js/formValidation.min.js"></script>
<script src="js/framework/bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#typeForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                txtCatName: {
                    validators: {
                        notEmpty: {
                            message: 'Không được để trống tên hãng sản xuất'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Tên hãng sản xuất phải dài từ 3 đến 30 ký tự'
                        }
                    }
                }
            }
        })
        .end()
});
</script>
JS;
?>

