<?php
require_once './inc_func.php';
require_once './dbHelper.php';

?>

<?php
    $sql = "select * from typeproducts";
    $rs = load($sql);
    $i = 1;
    while ($row = $rs->fetch_assoc()) {
        ?>
            <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $row["TypeName"]; ?></td>
                <td><?php echo $row["TypeID"]; ?></td>
                <td colspan="2">
                    <a href="admin.php?act=delete&id=<?php echo $row["TypeID"]; ?>" class="btn btn-default" aria-label="Left Align" title="Xóa loại sản phẩm">
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                    </a>
                    <a href="admin.php?act=edittype&id=<?php echo $row["TypeID"] ?>&name=<?php echo $row["TypeName"]; ?>" class="btn btn-default" aria-label="Left Align" title="Chỉnh sửa">
                        <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                    </a>
                </td>  
            </tr>
        <?php
        $i += 1;
    }
?>