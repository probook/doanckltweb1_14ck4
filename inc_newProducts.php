<?php
require_once './inc_func.php';
require_once './dbHelper.php';

?>

<?php
    $sql = "select * from products order by Day DESC";
    $rs = load($sql);
    $count = 1;
        while ($row = $rs->fetch_assoc()) {
            if($count <= 3)
            {
                ?>
                    <div class="border_box">
                        <div class="product_title"><?php echo $row["ProName"]; ?></div>
                        <div class="product_img"><a href="listProducts.php?act=details&id=<?php echo $row["ProID"]; ?>&name=<?php echo $row["ProName"]; ?>&type=<?php echo $row["ProType"]; ?>&cat=<?php echo $row["CatID"]; ?>&price=<?php echo number_format($row["Price"]-1900000); ?>"><img src="images/<?php echo $row["ProID"]; ?>/<?php echo $row["ProID"]; ?>_mini.jpg" alt="" border="0" width="100"/></a></div>
                        <div class="prod_price"><span class="reduce"><?php echo number_format($row["Price"]); ?></span> <span class="price"><?php echo number_format($row["Price"]-1900000); ?></span></div>
                    </div>
                    <hr/>
                <?php
            }
            $count+=1;
        }
        ?>
    <?php
?>