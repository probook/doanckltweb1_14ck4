<?php

require_once './dbHelper.php';

$sql = "select * from products where Price <= all (select MIN(Price) from products)";
$rs = load($sql);

while ($row = $rs->fetch_assoc()) {
    $id = $row["ProID"];
    $name = $row["ProName"];
    $price = $row["Price"];
    ?>
    <div class="product_title"><a href="listProducts.php?act=details&id=<?php echo $row["ProID"]; ?>&name=<?php echo $row["ProName"]; ?>&type=<?php echo $row["ProType"]; ?>&cat=<?php echo $row["CatID"]; ?>&price=<?php echo number_format($price-2000000); ?>"><?php echo $name; ?></a></div>
    <div class="product_img"><a href="listProducts.php?act=details&id=<?php echo $row["ProID"]; ?>&name=<?php echo $row["ProName"]; ?>&type=<?php echo $row["ProType"]; ?>&cat=<?php echo $row["CatID"]; ?>&price=<?php echo number_format($price-2000000); ?>"><img width="150" src="images/<?php echo $id; ?>/<?php echo $id; ?>_mini.jpg" alt="" border="0" /></a></div>
    <div class="prod_price"><span class="reduce"><?php echo number_format($price); ?> vnđ </span><span class="price"><?php echo number_format($price-2000000); ?> vnđ</span></div>
    <?php
}
?>