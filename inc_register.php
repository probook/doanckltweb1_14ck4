<?php
require_once './dbHelper.php';
require_once './inc_func.php';

if (isset($_POST["btnRegister"])) {


    //
    // kiểm tra captcha

    if (empty($_SESSION['captcha']) || trim(strtolower($_POST['txtCaptcha'])) != $_SESSION['captcha']) 
    {
        ?>
            <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <span>WRONG CAPTCHA</span>
            </div>
        <?php
    } else {
        $uid = $_POST["txtUID"];
        $pwd = $_POST["txtPWD"];
        $email = $_POST["txtEmail"];
        $enc_pwd = md5($pwd);

        $sql = "select * from users where f_Username = '$uid' or f_Email = '$email'";
        $rs = load($sql);    
        if($rs->num_rows > 0)
        {
            ?>
               <div float="right" class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <span>Tên đăng nhập hoặc Email đã tồn tại</span>
                </div>
            <?php
        }
        else
        {
            $fullname = $_POST["txtFullName"];
            $str_dob = $_POST["txtDOB"];
            $str_dob = str_replace('/', '-', $str_dob);
            $dob = strtotime($str_dob); //d-m-Y
            $str_dob = date('Y-m-d H:i:s', $dob);
            // echo $str_dob;

            $sql2 = "insert into users(f_Username, f_Password, f_Name, f_Email, f_DOB, f_Permission) values('$uid', '$enc_pwd', '$fullname', '$email', '$str_dob', 0)";

            $id = save($sql2, 0);
            echo $id;
            header("Location: index.php?act=profile");
        }
    }
}
?>

<div class="col-md-11 col-sm-offset-1">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Đăng ký người dùng</h3>
        </div>
        <div class="panel-body">
            <form class="form-horizontal" action="" method="post" id="registerForm" onsubmit="return validate();">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 title">
                        Thông tin đăng nhập
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="form-group">
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="txtUID" id="txtUID" onblur="bgcolor(this,1)" placeholder="Tên đăng nhập">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <input type="password" class="form-control" name="txtPWD" id="txtPWD" onblur="bgcolor(this,1)" placeholder="Mật khẩu">
                            </div>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" name="txtConfirmPWD" id="txtConfirmPWD" onblur="bgcolor(this,1)" placeholder="Nhập lại mật khẩu">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-10 col-md-offset-1 title">
                        Thông tin cá nhân
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="form-group">
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="txtFullName" id="txtFullName" onblur="bgcolor(this,1)" placeholder="Họ tên">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <input type="email" class="form-control" name="txtEmail" id="txtEmail" onblur="bgcolor(this,1)" placeholder="Email">
                            </div>
                            <div class="col-sm-6">                                                
                                <input type="text" class="form-control datepicker" name="txtDOB" id="txtDOB" onblur="bgcolor(this,1)" placeholder="Ngày sinh" readonly="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <img onclick="this.src = 'cool-php-captcha-0.3.1/captcha.php?' + Math.random();"  style="cursor: pointer" id="captchaImg" src="cool-php-captcha-0.3.1/captcha.php" />
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="txtCaptcha" name="txtCaptcha" onblur="bgcolor(this,1)" placeholder="Captcha">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-success pull-right" name="btnRegister">
                                    <i class="fa fa-check"></i> Đăng ký
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php
$js = <<<JS
<script src="assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script type="text/javascript">
    function validate() 
    {
        var uid = document.getElementById("txtUID");
        var pwd = document.getElementById("txtPWD");
        var cfpwd = document.getElementById("txtConfirmPWD");
        var fullname= document.getElementById("txtFullName");
        var email =document.getElementById("txtEmail");
        var dob = document.getElementById("txtDOB");
        var cap = document.getElementById("txtCaptcha");

        if(cap.value == "")
        {
            bgcolor(cap,2);
            alert("Chưa nhập Captcha");
            return false;
        }

        if(uid.value == "" && pwd.value == "" && cfpwd.value == "" && fullname.value == "" && dob.value == "")
        {
            bgcolor(uid,2);
            bgcolor(pwd,2);
            bgcolor(cfpwd,2);
            bgcolor(fullname,2);
            alert("Không được để trống");
            return false;
        }
        
        var str_dob = dob.value;
        var bd = new Date(str_dob).getFullYear();
        var current = new Date().getFullYear();
        var age = current - bd;
        if(age < 15)
        {
            alert("Độ tuổi không phù hợp");
            bgcolor(dob,2);
            return false;
        }

        if(uid_validation(uid,6,12))
        {
            if(pwd_validation(pwd,8,16))
            {
                if(pwd.value != cfpwd.value || cfpwd.value.length == 0)
                {
                    bgcolor(cfpwd,2);
                    alert("Mật khẩu nhập lại không trùng với mật khẩu");
                    return false;
                }
               else
               {
                    if(allLetter(fullname))
                   {
                        return true;
                   }
                   else
                   {
                        bgcolor(fullname,2);
                   }
               }

            }
            else
            {
                bgcolor(pwd,2);
            }
        }
        else
        {
            bgcolor(uid,2);
        }
        return false;
    } 

    function uid_validation(uid,mx,my)  
    {  
        var uid_len = uid.value.length;  
        if (uid_len == 0 || uid_len > my || uid_len < mx)  
        {  
            alert("Tên đăng nhập không được để trống / độ dài tên đăng nhập phải từ "+mx+" đến "+my+" ký tự ");  
            return false;  
        }  
        return true;  
    }

    function pwd_validation(pwd,mx,my)  
    {  
        var pwd_len = pwd.value.length;  
        if (pwd_len == 0 ||pwd_len > my || pwd_len < mx)  
        {  
            alert("Mật khẩu không được để trống / độ dài mật khẩu phải từ "+mx+" đến "+my+ " ký tự ");  
            return false;  
        }  
        return true;  
    } 

    function allLetter(uname)  
    {   
        var letters = /^[A-Za-z ][A-Za-z0-9!ÀáÂãÈÉÊÌÍÒóÔÕÙÚÝàáâãèéêìíòóôõùúýĂăĐđĨĩŨũƠơƯưẠạẢảẤấẦầẨẩẪẫẬậẹẸặẶẵẴẳẲằẰắẴẺẻẼẽẾếỀềỂểỄễỆệỈỉỊịỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞởỠỡỢợỤụỦủỨứỪừỬửỮữỰựỲỳỴỵỶỷỸỹ ]+$/;
        if(uname.value.match(letters))
        {
            return true; 
        }
        else
        {
            alert("Họ tên không được chứa ký tự đặc biệt");
            return false;
        }    
    }

    function bgcolor(a,flag)
    {
        switch (flag) {
            case 1:
                a.style.background = 'white';
                break;
            case 2:
                a.style.background = '#E87070';
                break;
        }
        
    }

    $(function () {
      $('.datepicker').datepicker({ format: "mm/dd/yyyy" }).on('changeDate', function (ev) {
        $(this).datepicker('hide');
      });
    });
</script>
JS;
?>


