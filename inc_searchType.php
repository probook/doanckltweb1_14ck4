<?php

require_once './dbHelper.php';

$sql = "select * from typeproducts";
$rs = load($sql);

if(isset($_GET["tab"]))
{
    $catid = $_GET["id"];
    $catname = $_GET["name"];

    while ($row = $rs->fetch_assoc()) {
    $id = $row["TypeID"];
    $name = $row["TypeName"];
    if($id%2!=0)
    {
        ?>
        <li id="<?php echo $id; ?>" name="<?php echo $id; ?>" class="odd"><a href="listProducts.php?act=products&tab=search&catid=<?php echo $catid; ?>&catname=<?php echo $catname; ?>&typeid=<?php echo $id; ?>&typename=<?php echo $name; ?>&page=1"><?php echo $name; ?></a></li>
        <?php
    }
    else
    {
        ?>
        <li id="<?php echo $id; ?>" name="<?php echo $id; ?>" class="even"><a href="listProducts.php?act=products&tab=search&catid=<?php echo $catid; ?>&catname=<?php echo $catname; ?>&typeid=<?php echo $id; ?>&typename=<?php echo $name; ?>&page=1"><?php echo $name; ?></a></li>
        <?php
    }
}
}
?>