<?php session_start(); 
      ob_start();?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Camera Store</title>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
    <link href="assets/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/font-awesome-4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/lightbox2/css/lightbox.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>
    <link href="css/hoverDropDown.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="style.css" />
    <<style type="text/css" media="screen">
    .center_prod_box{
      width: 173px;
      height: 173px;
      text-align:center;
    }
    .center_title_bar{
      padding:0 0 0 50px;
    }
    .right_content{
      float: left;
      margin-left: 40px;
    }
    .title_box{
      color:#fff;
    }
    a.compare{
      width:120px;
    }
    a.addtocart{
      width:120px;
    }
    #header{
      width:1000px;
      height:353px;
      background: url(images/header_bg.jpg) repeat-y center;
      background-size: 1000px 353px;
      margin:auto;
    }
    body
    {
      padding-top: 40px;
      background:url(images/bg.jpg) no-repeat #fff center top;
      color:#fff;
    }
    .specifications{
      color: #000000;
    }
    .prod_price_big{
      color: #000000;
    }
    .cart_details{
      color: #000000;
    }
    .navbar{
      margin-top: 0px;
    }
  </style>
    <!--[if IE 6]>
    <link rel="stylesheet" type="text/css" href="iecss.css" />
    <![endif]-->
    <script type="text/javascript" src="js/boxOver.js"></script>
  <script type="text/javascript" src="js/hoverDropDown.js"></script>
    <link rel="stylesheet" href="">
</head>
<body>
    <div id="main_container">
  <div class="top_bar">
     <?php include_once "./inc_top.php"; ?>
  </div>
  <div id="header">
    <div class="oferte_content">
    <div class='carousel slide' data-ride='carousel' id='carousel-example-generic'>
    <!-- Indicators -->
    <ol class='carousel-indicators'>
      <li class='active' data-slide-to='0' data-target='#carousel-example-generic'/>
      <li data-slide-to='1' data-target='#carousel-example-generic'/>
      <li data-slide-to='2' data-target='#carousel-example-generic'/>
      <li data-slide-to='3' data-target='#carousel-example-generic'/>
    </ol>

    <!-- Wrapper for slides -->
    <div class='carousel-inner' role='listbox'>
      <div class='item active'>
        <img alt='...' src='images/header_top01.jpg'/>
        <div class='carousel-caption'>
        </div>
      </div>
      <div class='item'>
        <img alt='...' src='images/header_top02.jpg'/>
        <div class='carousel-caption'>
        </div>
      </div>
      <div class='item'>
        <img alt='...' src='images/header_top03.jpg'/>
        <div class='carousel-caption'>
        </div>
      </div>
      <div class='item'>
        <img alt='...' src='images/header_top04.jpg'/>
        <div class='carousel-caption'>
        </div>
      </div>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
  <hr>
  <div id="main_content">
    <div class="left_content">
      <div class="title_box">Nhà sản xuất</div>
      <ul class="left_menu">
        <?php include_once "./inc_left.php"; ?>
      </ul>
      <div class="title_box">Loại máy ảnh</div>
      <ul class="left_menu">
        <?php include_once "./inc_midleft.php"; ?>
      </ul>
      <div class="title_box">Sản phẩm đặc biệt</div>
      <div class="border_box">
        <?php include_once "./inc_botLeft.php"; ?>
      </div>
    </div>
    <!-- end of left content -->
    <div class="center_content" >
        <?php
            if(isset($_GET["item"]))
            {
                include_once "./inc_search.php"; 
            }
        ?>
    </div>
    <!-- end of center content -->
    <div class="right_content">
      <?php 
        require_once './inc_func.php'; 
        if (isAuthenticated())
        {
            ?>
              <div class="shopping_cart">
                <div class="cart_title">Giỏ hàng</div>
                <div class="cart_details"> Có <?php echo cart_sum_items(); ?> sản phẩm <br />
                  <?php
                    $total = 0;
                    foreach ($_SESSION["cart"] as $id => $quantity) {
                        $sql = "select * from products where proid=$id";
                        $rs = load($sql);
                        $row = $rs->fetch_assoc();
                        $total += $row["Price"] * $quantity;
                    }
                    ?>
                  <span class="border_cart"></span> Tổng tiền: <span class="price"><?php echo number_format($total); ?> vnđ</span> </div>
                <div class="cart_icon"><a href="index.php?act=cart" title="header=[Xem giỏ hàng] body=[&nbsp;] fade=[on]"><img src="images/shoppingcart.png" alt="" width="48" height="48" border="0" /></a></div>
              </div>
            <?php
        }
      ?>
      <div class="title_box">Sản phẩm mới nhất</div>
        <?php include_once "./inc_newProducts.php";?>
    </div>
    <!-- end of right content -->
  </div>
  <!-- end of main content -->
</div>
  <script src="assets/jquery-3.1.1.min.js" type="text/javascript"></script>
  <script src="assets/bootstrap-3.3.7-dist/js/bootstrap.min.js" type="text/javascript"></script>
  <?php
    if (isset($js)) {
        echo $js;
    }
  ?>
</body>
</html>

