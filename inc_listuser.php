<?php
require_once './inc_func.php';
require_once './dbHelper.php';

if(isset($_POST["txtProId"])) {
    $sp = $_POST["txtProId"];
    $slg = 1;
    setCart($sp, $slg);
    print_r(getCart());
    redirect("index.php");
    ob_end_flush();
}

?>

<?php
    $u = $_SESSION["auth_user"];
    $sql = "select * from users";
    $rs = load($sql);
    $i = 1;
    while ($row = $rs->fetch_assoc()) {
        ?>
            <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $row["f_Username"]; ?></td>
                <td><?php echo $row["f_Name"]; ?></td>
                <td><?php echo $row["f_Email"]; ?></td>
                <td><?php echo $row["f_DOB"]; ?></td>
                <?php
                if($u["f_Permission"] == 2)
                {
                    if($row["f_Permission"] == 1 || $row["f_Permission"] == 2)
                    {
                        ?>
                            <td>Quản trị viên</td>
                            <td colspan="2">
                            </td>
                        <?php
                    }
                    else
                    {
                        ?>
                            <td>Khách hàng</td>
                            <td colspan="2">
                                <a href="admin.php?act=delete&uname=<?php echo $row["f_Username"]; ?>" class="btn btn-default" aria-label="Left Align" title="Xóa người dùng">
                                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                </a>
                                <a href="admin.php?act=edituser&id=<?php echo $row["f_ID"]; ?>&username=<?php echo $row["f_Username"]; ?>&name=<?php echo $row["f_Name"]; ?>&email=<?php echo $row["f_Email"]; ?>&dob=<?php echo $row["f_DOB"]; ?>" class="btn btn-default" aria-label="Left Align" title="Chỉnh sửa">
                                    <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                                </a>
                            </td>
                        <?php
                    }
                }
                else
                {
                    if($row["f_Permission"] == 1)
                    {
                        ?>
                            <td>Quản trị viên</td>
                            <td colspan="2">
                            </td>
                        <?php
                    }
                    else
                    {
                        if($row["f_Permission"] == 2)
                        {
                            ?>
                                <td>Quản trị viên (phụ)</td>
                                <td colspan="2">
                                    <a href="admin.php?act=delete&uname=<?php echo $row["f_Username"]; ?>" class="btn btn-default" aria-label="Left Align" title="Xóa người dùng">
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                    </a>
                                    <a href="admin.php?act=edituser&id=<?php echo $row["f_ID"]; ?>&username=<?php echo $row["f_Username"]; ?>&name=<?php echo $row["f_Name"]; ?>&email=<?php echo $row["f_Email"]; ?>&dob=<?php echo $row["f_DOB"]; ?>" class="btn btn-default" aria-label="Left Align" title="Chỉnh sửa">
                                        <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                                    </a>
                                </td>
                            <?php
                        }
                        else
                        {
                            ?>
                                <td>Khách hàng</td>
                                <td colspan="2">
                                    <a href="admin.php?act=delete&uname=<?php echo $row["f_Username"]; ?>" class="btn btn-default" aria-label="Left Align" title="Xóa người dùng">
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                    </a>
                                    <a href="admin.php?act=edituser&id=<?php echo $row["f_ID"]; ?>&username=<?php echo $row["f_Username"]; ?>&name=<?php echo $row["f_Name"]; ?>&email=<?php echo $row["f_Email"]; ?>&dob=<?php echo $row["f_DOB"]; ?>" class="btn btn-default" aria-label="Left Align" title="Chỉnh sửa">
                                        <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                                    </a>
                                </td>
                            <?php
                        }
                    } 
                }
                
                ?>
                
            </tr>
        <?php
        $i += 1;
    }
?>

<?php
$js = <<<JS
<script src="assets/lightbox2/js/lightbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
    function setProId(id) {
        f.txtProId.value = id;
        f.submit();
    }
</script>
JS;
?>