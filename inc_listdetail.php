<?php
require_once './inc_func.php';
require_once './dbHelper.php';

if(isset($_GET["view"]))
{
    $id = $_GET["id"];
    $sql = "select p.ProID, p.ProName, o.Price, o.Quantity, o.Amount from products p, orderdetails o where p.ProID = o.ProID and o.OrderID = $id";
    $rs = load($sql);
    $i = 1;
    while ($row = $rs->fetch_assoc()) {
        ?>
            <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $row["ProName"]; ?></td>
                <td><?php echo number_format($row["Price"]); ?></td>
                <td><?php echo $row["Quantity"]; ?></td>
                <td><?php echo number_format($row["Amount"]); ?></td>            
            </tr>
        <?php
        $i += 1;
    }
}

?>

