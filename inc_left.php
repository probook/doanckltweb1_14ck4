
<?php

require_once './dbHelper.php';

$sql = "select * from categories";
$rs = load($sql);

while ($row = $rs->fetch_assoc()) {
    $id = $row["CatID"];
    $name = $row["CatName"];
    if($id%2!=0)
    {
        ?>
        <li class="odd"><a href="listProducts.php?tab=search&act=products&id=<?php echo $id; ?>&name=<?php echo $name; ?>&page=1"><?php echo $name; ?></a></li>
        <?php
    }
    else
    {
        ?>
        <li class="even"><a href="listProducts.php?tab=search&act=products&id=<?php echo $id; ?>&name=<?php echo $name; ?>&page=1"><?php echo $name; ?></a></li>
        <?php
    }
}
?>