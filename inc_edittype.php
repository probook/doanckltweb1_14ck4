<?php
require_once './dbHelper.php';
require_once './inc_func.php';

if(isset($_GET["id"]))
{
    $id = $_GET["id"];
    $sql = "select * from typeproducts where TypeID = $id";
    $rs = load($sql);
    $row = $rs->fetch_assoc();
    $u["TypeName"] = $row["TypeName"];
}

if (isset($_POST["btnUpdate"])) {
    $id = $_POST["txtTypeID"];
    $typename = $_POST["txtTypeName"];

    $sql = "Update typeproducts set TypeName = '$typename' where TypeID = '$id'";
    $n = save($sql,1);

    redirect("admin.php?act=typeproducts");
}
?>

<form id="typeForm" method="post" class="form-horizontal">
    <input type="hidden" class="form-control" name="txtTypeID" id="txtTypeID" value="<?php echo $id; ?>"/>
    <div class="form-group">
        <label class="col-xs-4 control-label">Tên loại sản phẩm</label>
        <div class="col-xs-5">
            <input type="text" class="form-control" name="txtTypeName" id="txtTypeName" value="<?php echo $u["TypeName"]; ?>" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-4 col-xs-offset-4">
            <button type="submit" class="btn btn-default" name="btnUpdate" id="btnUpdate">Update</button>
        </div>
    </div>
</form>

<?php
$js = <<<JS
<script src="js/formValidation.min.js"></script>
<script src="js/framework/bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#typeForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                txtTypeName: {
                    validators: {
                        notEmpty: {
                            message: 'Không được để trống tên loại sản phẩm'
                        },
                        stringLength: {
                            min: 6,
                            max: 50,
                            message: 'Tên loại sản phẩm phải dài từ 6 đến 50 ký tự'
                        }
                    }
                }
            }
        })
        .end()
});
</script>
JS;
?>

