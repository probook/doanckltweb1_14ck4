<?php
require_once './inc_func.php';
require_once './dbHelper.php';

if(isset($_POST["txtProId"])) {
    $sp = $_POST["txtProId"];
    $typeID = $_POST["txtTypeId"];
    $typeName = $_POST["txtTypeName"];
    $num_page = $_POST["txtPage"];
    $slg = 1;
    setCart($sp, $slg);
    print_r(getCart());
    redirect("listProducts.php?act=type&id=$typeID&name=$typeName&page=$num_page");
    ob_end_flush();
}

?>

<?php
    if (isset($_GET["id"])) {
        $id = $_GET["id"];
        $name = $_GET["name"];
        $page = $_GET["page"];
        ?>
            <form id="f" action="" method="post">
                <input type="hidden" id="txtProId" name="txtProId"/>
                <input type="hidden" id="txtTypeId" name="txtTypeId" value="<?php echo $id; ?>" />
                <input type="hidden" id="txtTypeName" name="txtTypeName" value="<?php echo $name; ?>" />
                <input type="hidden" id="txtPage" name="txtPage" value="<?php echo $page; ?>" />
            </form>
        <?php
        if($page == "1")
        {
            $page1 = 0;
        }
        else
        {
            $page1 = ($page*6)-6;
        }
        $sql = "select * from products where ProType = $id limit $page1,6";
        $rs = load($sql);
        $sql1 = "select * from products where ProType = $id";
        $rs1 = load($sql1);
        $num_page = ceil($rs1->num_rows/6);
        ?>
        <div class="center_title_bar"><?php echo $name; ?></div>
        <?php
            while ($row = $rs->fetch_assoc()) {
                ?>
                    <div class="prod_box">
                        <div class="center_prod_box">
                          <div class="product_title"><a href="listProducts.php?act=details&id=<?php echo $row["ProID"]; ?>&name=<?php echo $row["ProName"]; ?>&type=<?php echo $row["ProType"]; ?>&cat=<?php echo $row["CatID"]; ?>"><?php echo $row["ProName"]; ?></a><br/></div>
                          <div class="product_img"><a href="listProducts.php?act=details&id=<?php echo $row["ProID"]; ?>&name=<?php echo $row["ProName"]; ?>&type=<?php echo $row["ProType"]; ?>&cat=<?php echo $row["CatID"]; ?>"><img src="images/<?php echo $row["ProID"];?>/<?php echo $row["ProID"];?>_mini.jpg" alt="" border="0" width="100" /></a></div>
                          <div class="prod_price"><span class="price"><?php echo number_format($row["Price"]); ?> vnđ</span></div>
                        </div>
                        <div class="bottom_prod_box"></div>
                        <div class="prod_details_tab"> 
                        <?php
                        if(isAuthenticated())
                        {
                            ?>
                                <a href="#" role="button" onclick="setProId(<?php echo $row["ProID"]; ?>)" title="header=[Đặt hàng] body=[&nbsp;] fade=[on]"><img src="images/cart.gif" alt="" border="0" class="left_bt" /></a>
                            <?php
                        } 
                        ?>
                        <a href="listProducts.php?act=details&id=<?php echo $row["ProID"]; ?>&name=<?php echo $row["ProName"]; ?>&type=<?php echo $row["ProType"]; ?>&cat=<?php echo $row["CatID"]; ?>" class="prod_details">details</a> </div>
                    </div>
                <?php
            }
            ?><div class="container"><ul class="pagination"><?php
            for($i = 1; $i <= $num_page; $i++)
            {
                ?>
                    <li><a href="listProducts.php?act=type&id=<?php echo $id; ?>&name=<?php echo $name; ?>&page=<?php echo $i; ?>" title=""><?php echo $i; ?></a></li>
                <?php
            }
            ?></ul></div><?php
    } else {
        redirect("index.php");
    }
?>

<?php
$js = <<<JS
<script src="assets/lightbox2/js/lightbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
    function setProId(id) {
        f.txtProId.value = id;
        f.submit();
    }
</script>
JS;
?>