<?php
require_once './inc_func.php';
require_once './dbHelper.php';

?>

<?php
    $sql = "select * from products";
    $rs = load($sql);
    $i = 1;
    while ($row = $rs->fetch_assoc()) {
        ?>
            <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $row["ProName"]; ?></td>
                <td><?php echo number_format($row["Price"]); ?></td>
                <td><?php echo $row["Quantity"]; ?></td>
                <?php
                    $str_day = $row["Day"];
                    $day = strtotime($str_day);
                    $str_day = date('d-m-Y',$day); 
                 ?>
                <td><?php echo $str_day; ?></td>
                <td colspan="2">
                    <a href="admin.php?act=delete&pid=<?php echo $row["ProID"]; ?>" class="btn btn-default" aria-label="Left Align" title="Xóa sản phẩm">
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                    </a>
                    <a href="admin.php?act=editproduct&pid=<?php echo $row["ProID"] ?>&pname=<?php echo $row["ProName"]; ?>" class="btn btn-default" aria-label="Left Align" title="Chỉnh sửa">
                        <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                    </a>
                </td>  
            </tr>
        <?php
        $i += 1;
    }
?>