<?php session_start(); 
      ob_start();?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Camera Store</title>
  <meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
  <link href="assets/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
  <link href="css/formValidation.min.css" rel="stylesheet" type="text/css"/>
  <link href="css/profile.css" rel="stylesheet" type="text/css"/>
  <link href="css/editprofile.css" rel="stylesheet" type="text/css"/>
  <link href="assets/font-awesome-4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
  <link href="assets/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>
  <link href="assets/lightbox2/css/lightbox.min.css" rel="stylesheet" type="text/css"/>
  <link href="css/hoverDropDown.css" rel="stylesheet" type="text/css"/>
  <link href="css/viewright.css" rel="stylesheet" type="text/css"/>
  <link href="css/boostrap-combobox.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" type="text/css" href="style.css" />
  <style type="text/css" media="screen">
    .center_prod_box{
      width: 173px;
      height: 173px;
      text-align:center;
    }
    .center_title_bar{
      padding:0 0 0 50px;
    }
    .right_content{
      float: left;
      margin-left: 40px;
    }
    .title_box{
      color:#fff;
    }
    #header{
      width:1000px;
      height:353px;
      background: url(images/header_bg.jpg) repeat-y center;
      background-size: 1000px 353px;
      margin:auto;
    }
    body
    {
      padding-top: 55px;
      background:url(images/bg.jpg) no-repeat #fff center top;
      color:#fff;
    }
    .cart_details{
      color: #000000;
    }
    .title 
    {
      font-family: 'Open Sans';
      font-size: 16pt;
      padding-top: 10px;
      padding-bottom: 10px;
      border-bottom: 1px solid #cccccc;
      margin-bottom: 35px;
      color: black;
    }
    
    .datepicker{
      width: 200px;
    }
    .datepicker td,
    .datepicker th {
      min-width: 20px;
      -webkit-text-fill-color: black;
    }
    .datepicker td.old,
    .datepicker td.new {
      -webkit-text-fill-color: #868686;
    }
    table td,
          th{
      min-width: 110px; 
      text-align: center;
      -webkit-text-fill-color: black;
    }
    .div-square {
        padding:5px;
        border:3px double #e1e1e1;
        -webkit-border-radius:8px;
       -moz-border-radius:8px;
        border-radius:8px;
        margin:5px;
        text-align: center;
        height: 120px;
    }
    .div-square> a,.div-square> a:hover {
        color: #3AC7AE;
        text-decoration: none;
    }
    .control-label {
      color: black;
    }
    .navbar{
      margin-top: 0px;
    }
    h1{
      color: gray;
    }
  </style>
  <!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="iecss.css" />
  <![endif]-->
  <script type="text/javascript" src="js/boxOver.js"></script>
  <script type="text/javascript" src="js/hoverDropDown.js"></script>
  <script type="text/javascript" src="js/lumino.glyphs.js"></script>
  <script type="text/javascript" src="js/bootstrap-combobox.js"></script>
  <style type="text/css">
    /* Adjust feedback icon position */
    #productForm .selectContainer .form-control-feedback,
    #productForm .inputGroupContainer .form-control-feedback {
        right: -15px;
    }
  </style>
</head>
<body>
  <div id="main_container">
  <div class="top_bar">
     <?php include_once "./inc_top.php"; ?>
  </div>
  <div id="header">
    <div class="oferte_content">
    <div class='carousel slide' data-ride='carousel' id='carousel-example-generic'>
    <!-- Indicators -->
    <ol class='carousel-indicators'>
      <li class='active' data-slide-to='0' data-target='#carousel-example-generic'/>
      <li data-slide-to='1' data-target='#carousel-example-generic'/>
      <li data-slide-to='2' data-target='#carousel-example-generic'/>
      <li data-slide-to='3' data-target='#carousel-example-generic'/>
    </ol>

    <!-- Wrapper for slides -->
    <div class='carousel-inner' role='listbox'>
      <div class='item active'>
        <img alt='' src='images/header_top01.jpg'/>
        <div class='carousel-caption'>
        </div>
      </div>
      <div class='item'>
        <img alt='' src='images/header_top02.jpg'/>
        <div class='carousel-caption'>
        </div>
      </div>
      <div class='item'>
        <img alt='' src='images/header_top03.jpg'/>
        <div class='carousel-caption'>
        </div>
      </div>
      <div class='item'>
        <img alt='' src='images/header_top04.jpg'/>
        <div class='carousel-caption'>
        </div>
      </div>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
  <hr>
  <div id="main_content">
    <div class="left_content">
      <?php
          if(isset($_GET["person"]))
          {
            $u = $_SESSION["auth_user"];
            ?>                      
                      <div class="profile-sidebar">
                        <!-- SIDEBAR USERPIC -->
                        <div class="profile-userpic">
                            <img src="images/avatar.png" class="img-responsive" alt="">           
                        </div>
                        <!-- END SIDEBAR USERPIC -->
                        <!-- SIDEBAR USER TITLE -->
                        <div class="profile-usertitle">
                          <div class="profile-usertitle-name">
                            <span class="hidden-xs"><?php echo $u["f_Name"] ?></span>
                          </div>
                          <div class="profile-usertitle-job">
                            <span class="hidden-xs">Khách hàng</span>
                          </div>
                        </div>
                        <!-- END SIDEBAR USER TITLE -->
                        <!-- SIDEBAR MENU -->
                        <div class="profile-usermenu">
                          <ul class="nav">
                            <li>
                              <a href="index.php?act=profile&person=<?php echo $u["f_ID"] ?>">
                              <i class="glyphicon glyphicon-home"></i>
                              <span class="hidden-xs">Thông tin cá nhân<span> </a>
                            </li>
                            <br>
                            <li>
                              <a href="index.php?act=edit&person=<?php echo $u["f_ID"] ?>">
                              <i class="glyphicon glyphicon-user"></i>
                              <span class="hidden-xs">Cập nhật thông tin<span> </a>
                            </li>
                            <br>
                            <li>
                              <a href="index.php?act=cart">
                              <i class="glyphicon glyphicon-shopping-cart"></i>
                              <span class="hidden-xs">Xem giỏ hàng<span> </a>         
                            </li>
                                        
                          </ul>
                        </div>
                        <!-- END MENU -->
                      </div>
            <?php
          } 
          else
          {
            if(isAuthenticated())
            {
              if($u["f_Permission"] == 1 || $u["f_Permission"] == 2)
              {
                    $u = $_SESSION["auth_user"];
                    ?>                      
                        <div class="profile-sidebar">
                          <!-- SIDEBAR USERPIC -->
                          <div class="profile-userpic">
                              <img src="images/avatar.png" class="img-responsive" alt="">           
                          </div>
                          <!-- END SIDEBAR USERPIC -->
                          <!-- SIDEBAR USER TITLE -->
                          <div class="profile-usertitle">
                            <div class="profile-usertitle-name">
                              <span class="hidden-xs"><?php echo $u["f_Name"] ?></span>
                            </div>
                            <div class="profile-usertitle-job">
                              <span class="hidden-xs">Quản trị viên</span>
                            </div>
                          </div>
                          <!-- END SIDEBAR USER TITLE -->
                          <!-- SIDEBAR MENU -->
                          <div class="profile-usermenu">
                            <ul class="nav">
                              <li>
                                <a href="index.php?act=admin&Dashboard=access">
                                <i class="glyphicon glyphicon-credit-card"></i>
                                <span class="hidden-xs">DashBoard<span> </a>         
                              </li>
                              <li>
                                <a href="admin.php?act=user">
                                <i class="fa fa-users"></i>
                                <span class="hidden-xs"><b>Người dùng</b><span> </a>         
                              </li>
                              <li>
                                <a href="admin.php?act=products">
                                <i class="fa fa-archive"></i>
                                <span class="hidden-xs"><b>Sản phẩm</b><span> </a>         
                              </li>
                              <li>
                                <a href="admin.php?act=typeproducts">
                                <i class="fa fa-list-ol"></i>
                                <span class="hidden-xs"><b>Loại sản phẩm</b><span> </a>         
                              </li>
                              <li>
                                <a href="admin.php?act=categories">
                                <i class="fa fa-building-o"></i>
                                <span class="hidden-xs"><b>Nhà sản xuất</b><span> </a>         
                              </li>
                              <li>
                                <a href="admin.php?act=orders">
                                <i class="fa fa-clipboard"></i>
                                <span class="hidden-xs"><b>Đơn hàng</b><span> </a>         
                              </li>
                              <br>
                              <li>
                                <a href="index.php?act=profile">
                                <i class="glyphicon glyphicon-home"></i>
                                <span class="hidden-xs">Thông tin cá nhân<span> </a>
                              </li>
                              <br>
                              <li>
                                <a href="index.php?act=edit&person=<?php echo $u["f_ID"] ?>">
                                <i class="glyphicon glyphicon-user"></i>
                                <span class="hidden-xs">Cập nhật thông tin<span> </a>
                              </li>        
                            </ul>
                          </div>
                          <!-- END MENU -->
                        </div>
                    <?php
              }
              else
              {
                ?>
                    <div class="title_box">Nhà sản xuất</div>
                      <ul class="left_menu">
                        <?php include_once "./inc_left.php"; ?>
                      </ul>
                      <div class="title_box">Loại máy ảnh</div>
                      <ul class="left_menu">
                        <?php include_once "./inc_midleft.php"; ?>
                      </ul>
                      <div class="title_box">Sản phẩm đặc biệt</div>
                      <div class="border_box">
                        <?php include_once "./inc_botLeft.php"; ?>
                    </div>
                <?php
              }
            }
            else
            {
              ?>
                  <div class="title_box">Nhà sản xuất</div>
                    <ul class="left_menu">
                      <?php include_once "./inc_left.php"; ?>
                    </ul>
                    <div class="title_box">Loại máy ảnh</div>
                    <ul class="left_menu">
                      <?php include_once "./inc_midleft.php"; ?>
                    </ul>
                    <div class="title_box">Sản phẩm đặc biệt</div>
                    <div class="border_box">
                      <?php include_once "./inc_botLeft.php"; ?>
                  </div>
              <?php
            }
          }
       ?>
    </div>
    <?php
        if(isset($_GET["act"]))
        {
          $act = $_GET["act"];
          switch ($act) {
            case "user":
              ?>
                  <div class="col-lg-9">
                      <table class="table table-striped table-bordered table-hover" >
                          <thead>
                              <tr>
                                  <th>STT</th>
                                  <th>Tên đăng nhập</th>
                                  <th>Họ tên</th>
                                  <th>Email</th>
                                  <th>Ngày sinh</th>
                                  <th>Loại người dùng</th>
                                  <th>Thao tác</th>
                              </tr>
                          </thead>
                          <tbody>
                              <?php include_once "./inc_listuser.php"; ?>
                          </tbody>
                      </table>

                  </div>
              <?php
              break;
            case "edituser":
              ?>
                  <div class="center_content">
                      <?php include_once "./inc_edituser.php"; ?>
                  </div>
              <?php
              break;
            case "delete":
              include_once "./inc_delete.php";
              break;
            case "products":
              ?>
                  <div class="col-lg-9">
                      <a href="admin.php?act=insertproduct" class="btn btn-default" aria-label="Left Align" title="Thêm sản phẩm">
                        <label>Thêm sản phẩm mới</label>
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                      </a>
                      <table class="table table-striped table-bordered table-hover" >
                          <thead>
                              <tr>

                                  <th>STT</th>
                                  <th>Tên sản phẩm</th>
                                  <th>Giá</th>
                                  <th>Số lượng</th>
                                  <th>Ngày nhập</th>
                                  <th>Thao tác</th>
                              </tr>
                          </thead>
                          <tbody>
                              <?php include_once "./inc_listproduct.php"; ?>
                          </tbody>
                      </table>
                  </div>
                    
              <?php
              break;
            case "typeproducts":
              ?>
                  <div class="col-lg-9">
                      <a href="admin.php?act=inserttype" class="btn btn-default" aria-label="Left Align" title="Thêm sản phẩm">
                        <label>Thêm loại mới</label>
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                      </a>
                      <table class="table table-striped table-bordered table-hover" >
                          <thead>
                              <tr>

                                  <th>STT</th>
                                  <th>Tên loại</th>
                                  <th>Mã loại</th>
                                  <th>Thao tác</th>
                              </tr>
                          </thead>
                          <tbody>
                              <?php include_once "./inc_listtype.php"; ?>
                          </tbody>
                      </table>
                  </div>
                    
              <?php
              break;
            case "categories":
              ?>
                  <div class="col-lg-9">
                      <a href="admin.php?act=insertcat" class="btn btn-default" aria-label="Left Align" title="Thêm sản phẩm">
                        <label>Thêm hãng sản xuất</label>
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                      </a>
                      <table class="table table-striped table-bordered table-hover" >
                          <thead>
                              <tr>

                                  <th>STT</th>
                                  <th>Tên hãng sản xuất</th>
                                  <th>Mã hãng sản xuất</th>
                                  <th>Thao tác</th>
                              </tr>
                          </thead>
                          <tbody>
                              <?php include_once "./inc_listcat.php"; ?>
                          </tbody>
                      </table>
                  </div>
                    
              <?php
              break;
            case "orders":
              ?>
                  <div class="page-header">
                    <h1>Danh sách đơn hàng</h1>
                  </div>
                  <div class="col-lg-9">
                      <table class="table table-striped table-condensed table-hover" >
                          <thead>
                              <tr>

                                  <th>STT</th>
                                  <th>Tên khách hàng</th>
                                  <th>Ngày mua</th>
                                  <th>Tổng tiền</th>
                                  <th>Trạng thái</th>
                              </tr>
                          </thead>
                          <tbody>
                              <?php include_once "./inc_listorder.php"; ?>
                          </tbody>
                      </table>
                  </div>
                  <?php
                    if(isset($_GET["view"]))
                    {
                      ?>
                        <div class="col-lg-9">
                          <div class="page-header">
                            <h1>Chi tiết đơn hàng</h1>
                          </div>
                            <table class="table table-striped table-condensed table-hover" >
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên sản phẩm</th>
                                        <th>Đơn giá</th>
                                        <th>Số lượng</th>
                                        <th>Thành tiền</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php include_once "./inc_listdetail.php"; ?>
                                </tbody>
                            </table>
                        </div>  
                      <?php
                    } 
                   ?>
                  
                <?php
              break;
            case "editproduct":
              ?>
                  <div class="center_content">
                      <?php include_once "./inc_editproduct.php"; ?>
                  </div>
              <?php
              break;
            case "insertproduct":
              ?>
                  <div class="center_content">
                      <?php include_once "./inc_insertproduct.php"; ?>
                  </div>
              <?php
              break;
            case "edittype":
              ?>
                  <div class="center_content">
                      <?php include_once "./inc_edittype.php"; ?>
                  </div>
              <?php
              break;
            case "inserttype":
              ?>
                  <div class="center_content">
                      <?php include_once "./inc_inserttype.php"; ?>
                  </div>
              <?php
              break;
            case "editcat":
              ?>
                  <div class="center_content">
                      <?php include_once "./inc_editcat.php"; ?>
                  </div>
              <?php
              break;
            case "insertcat":
              ?>
                  <div class="center_content">
                      <?php include_once "./inc_insertcat.php"; ?>
                  </div>
              <?php
              break;
            
            default:
              # code...
              break;
          }

        } 
     ?>
  <!-- end of main content -->
</div>
  <script src="assets/jquery-3.1.1.min.js" type="text/javascript"></script>
  <script src="assets/bootstrap-3.3.7-dist/js/bootstrap.min.js" type="text/javascript"></script>
  <?php
    if (isset($js)) {
        echo $js;
    }
  ?>
</body>
</html>