<?php
require_once './dbHelper.php';
require_once './inc_func.php';

if (isset($_POST["btnInsert"])) {
    $proid = $_POST["txtProID"];
    $proname = $_POST["txtProName"];
    $str_day = $_POST["txtDay"];
    $quantity = $_POST["txtQuantity"];
    $tinydes = $_POST["txtTinyDes"];
    $fulldes = $_POST["txtFullDes"];
    $price = $_POST["txtPrice"];
    $type = $_POST["txtType"];
    $cat = $_POST["txtCat"];

    $str_day = str_replace('/', '-', $str_day);
    $day = strtotime($str_day); //d-m-Y
    $str_day = date('Y-m-d H:i:s',$day);

    $sql = "Insert into products (ProName, ProType, TinyDes, FullDes, Price, CatID, Quantity, Day)
            values ('$proname','$type','$tinydes', '$fulldes', '$price', '$cat', '$quantity', '$str_day')";
    $n = save($sql,1);

    if(!is_dir("images/$proid"))
    {
        mkdir("images/$proid",0777,true);
    }
    
    $targetDir = "images/$proid/";
    $miniFle = $proid."_mini.jpg";
    $bigFle = $proid.".jpg";
    $_FILES["photoFile"]["name"] = $miniFile;
    $_FILES["photoFile2"]["name"] = $bigFile;

    $relativePath = $targetDir . $_FILES["photoFile"]["name"];
    $relativePath2 = $targetDir . $_FILES["photoFile2"]["name"];
    if(file_exists($relativePath))
    {
        echo "<div>" . $_FILES["photoFile"]["name"] . "đã tồn tại.</div>";
        echo "<br/>";

        echo "<a href='" .$_SERVER['PHP_SELF'] . "'>Thử lại</a>";
        echo "<a href='#' onclick='window.close();'>Đóng</a>";
    }
    else
    {
        if(file_exists($relativePath2))
        {
            echo "<div>" . $_FILES["photoFile2"]["name"] . "đã tồn tại.</div>";
            echo "<br/>";

            echo "<a href='" .$_SERVER['PHP_SELF'] . "'>Thử lại</a>";
            echo "<a href='#' onclick='window.close();'>Đóng</a>";
        }
        else
        {
            move_uploaded_file($_FILES["photoFile"]["tmp_name"], $relativePath);
            move_uploaded_file($_FILES["photoFile2"]["tmp_name"], $relativePath2);
            echo "<br/>";
        }
    } 

    redirect("admin.php?act=products");
}
?>

<form id="productForm" method="post" class="form-horizontal" enctype="multipart/form-data">
    <?php
        $sqld = "select count(ProID) from products";
        $rsd = load($sqld);
        $rowd = $rsd->fetch_assoc(); 
     ?>
    <input type="hidden" class="form-control" name="txtProID" id="txtProID" value="<?php echo $rowd["count(ProID)"]+1; ?>"/>
    <div class="form-group">
        <label class="col-xs-4 control-label">Tên sản phẩm</label>
        <div class="col-xs-5">
            <input type="text" class="form-control" name="txtProName" id="txtProName" value="" />
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-4 control-label">Ngày nhập</label>
        <div class="col-xs-5">
            <input type="text" required placeholder="Ngày nhập" value="" id="txtDay" name="txtDay" class="form-control datepicker" readonly="true">
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-4 control-label">Số lượng</label>
        <div class="col-xs-5">
            <input type="text" class="form-control" name="txtQuantity" id="txtQuantity" value="" />
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-4 control-label">Ảnh nhỏ</label>
        <div class="col-xs-5">
            <input type="file" class="form-control" name="photoFile" id="photoFile" />
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-4 control-label">Ảnh lớn</label>
        <div class="col-xs-5">
            <input type="file" class="form-control" name="photoFile2" id="photoFile2" />
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-4 control-label">Mô tả ngắn</label>
        <div class="col-xs-8">
            <textarea name="txtTinyDes" id="txtTinyDes" class="form-control" rows="2" ></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-4 control-label">Thông số kỹ thuật</label>
        <div class="col-xs-8">
            <textarea name="txtFullDes" id="txtFullDes" class="form-control" rows="5"></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-4 control-label">Giá</label>
        <div class="col-xs-5 inputGroupContainer">
            <div class="input-group">
                <input type="text" class="form-control" name="txtPrice" id="txtPrice" value="" />
                <span class="input-group-addon">vnđ</span>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="col-xs-4 control-label">Loại máy ảnh</label>
        <div class="col-xs-5 selectContainer">
            <select class="form-control" name="txtType" id="txtType">
                <option value="">Chọn một loại</option>
                <?php
                    $sqlt = "select * from typeproducts";
                    $rst = load($sqlt);
                    while ($rowt = $rst->fetch_assoc()) {
                        ?>
                            <option value="<?php echo $rowt["TypeID"] ?>"><?php echo $rowt["TypeName"] ?></option>
                        <?php
                     } 
                 ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-4 control-label">Nhà sản xuất</label>
        <div class="col-xs-5 selectContainer">
            <select class="form-control" name="txtCat" id="txtCat">
                <option value="">Chọn một nhà sản xuất</option>
                <?php
                    $sqlc = "select * from Categories";
                    $rsc= load($sqlc);
                    while ($rowc = $rsc->fetch_assoc()) {
                        ?>
                            <option value="<?php echo $rowc["CatID"] ?>"><?php echo $rowc["CatName"] ?></option>
                        <?php
                     } 
                 ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-4 col-xs-offset-3">
            <button type="submit" class="btn btn-default" name="btnInsert" id="btnInsert">Thêm</button>
        </div>
    </div>
</form>

<?php
$js = <<<JS
<script src="js/formValidation.min.js"></script>
<script src="js/framework/bootstrap.min.js"></script>
<script src="assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
      $('.datepicker').datepicker({ format: "dd/mm/yyyy" }).on('changeDate', function (ev) {
        $(this).datepicker('hide');
      });
    });
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#productForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                'txtProName': {
                    validators: {
                        notEmpty: {
                            message: 'Không được để trống tên sản phẩm'
                        },
                        stringLength: {
                            min: 6,
                            max: 50,
                            message: 'Tên sản phẩm phải dài từ 6 đến 50 ký tự'
                        }
                    }
                },
                txtDay: {
                    validators: {
                        notEmpty: {
                            message: 'Không được để trống ngày nhập'
                        },   
                    }
                },
                txtQuantity: {
                    validators: {
                        notEmpty: {
                            message: 'Không được để trống số lượng sản phẩm'
                        },
                        numeric: {
                            message: 'Số lượng phải là số'
                        }
                    }
                },
                photoFile: {
                    validators: {
                        notEmpty: {
                            message: 'Hãy chọn hình cho sản phẩm'
                        },
                        file: {
                            extension: 'jpeg,jpg,png',
                            type: 'image/jpeg,image/png',
                            minSize: 40000,   // 200*200
                            message: 'Kích thước file tối thiểu là 200 x 200'
                        }
                    }
                },
                photoFile2: {
                    validators: {
                        notEmpty: {
                            message: 'Hãy chọn hình cho sản phẩm'
                        },
                        file: {
                            extension: 'jpeg,jpg,png',
                            type: 'image/jpeg,image/png',
                            minSize: 250000,   // 500*500
                            message: 'Kích thước file tối thiểu là 500 x 500'
                        }
                    }
                },
                txtTinyDes: {
                    validators: {
                        notEmpty: {
                            message: 'Mô tả không được để trống'
                        },
                        stringLength: {
                            min: 10,
                            max: 100,
                            message: 'Nội dung mô tả phải có độ dài từ 10 đến 100 ký tự'
                        }
                    }
                },
                txtFullDes: {
                    validators: {
                        notEmpty: {
                            message: 'Thông số kỹ thuật không được để trống'
                        },
                        stringLength: {
                            min: 50,
                            max: 100000,
                            message: 'Nội dung này phải dài từ 50 đến 100000 ký tự'
                        }
                    }
                },
                txtPrice: {
                    validators: {
                        notEmpty: {
                            message: 'Không được để trống giá sản phẩm'
                        },
                        numeric: {
                            message: 'Giá sản phẩm phải là số'
                        }
                    }
                },
                txtType: {
                    validators: {
                        notEmpty: {
                            message: 'Không được để trống loại máy'
                        }
                    }
                },
                txtCat: {
                    validators: {
                        notEmpty: {
                            message: 'Không được để trống nhà sản xuất'
                        }
                    }
                }
            }
        })
        /* Using Combobox for Categories and typeproducts select elements */
        .find('[name="txtCat"], [name="txtType"]')
            .combobox()
            .end()
});
</script>
JS;
?>

